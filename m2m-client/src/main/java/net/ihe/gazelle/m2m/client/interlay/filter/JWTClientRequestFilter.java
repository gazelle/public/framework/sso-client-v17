package net.ihe.gazelle.m2m.client.interlay.filter;

import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.ext.Provider;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * This class is used to filter client request and therefore only works on classes annotated @RegisterRestClient or
 * created with QuarkusRestClientBuilder
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class JWTClientRequestFilter implements ClientRequestFilter {

    Logger log = LoggerFactory.getLogger(JWTClientRequestFilter.class);

    @Inject // Unable to use injection by constructor here, breaks UTs
    AccessTokenService accessTokenService;

    /**
     * This filter will add request a JWT from an access token provider and add it as an Authorization header before
     * the request is sent.
     *
     * @param requestContext request context.
     */
    @Override
    public void filter(ClientRequestContext requestContext) {
        final Method method = (Method) requestContext
                .getProperty("org.eclipse.microprofile.rest.client.invokedMethod");

        if (method != null) {
            M2MAuthMethod m2MAuthMethod = method.getAnnotation(M2MAuthMethod.class);
            if (m2MAuthMethod != null) {
                log.debug("M2M authentication annotation detected, retrieving JWT to add to request...");

                // Retrieve JWT
                String accessToken = accessTokenService.getAccessToken();
                requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
            } else {
                log.debug("No M2MAuthClient annotation detected, no JWT added to request");
            }
        }
    }
}
