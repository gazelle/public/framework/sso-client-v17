package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.Authz;
import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public final class AuthzImpl implements Authz {
   private final PermissionStore allPermissions;

   public AuthzImpl(PermissionStoreProvider provider) {
      this.allPermissions = new HolderPermissionStore(
            provider.getPermissionStores().stream()
            .flatMap(store -> store.getPermissions().stream())
            .collect(Collectors.toSet())
      );
   }

   @Override
   public boolean isAuthorized(GazelleIdentity identity, String action, Object... context) {
      return getPermission(action).isGranted(identity, context);
   }

   @Override
   public <C extends Collection<? extends Object>> C filterOutUnauthorized(GazelleIdentity identity,
                                                                           String action, C collection) {
      Permission permission = getPermission(action);
      collection.removeIf(object -> !permission.isGranted(identity, object));
      return collection;
   }

   private Permission getPermission(String action) {
      return allPermissions.getPermission(action);
   }

   private record HolderPermissionStore(Set<Permission> permissions) implements PermissionStore {
      @Override
      public Set<Permission> getPermissions() {
         return permissions;
      }
   }
}
