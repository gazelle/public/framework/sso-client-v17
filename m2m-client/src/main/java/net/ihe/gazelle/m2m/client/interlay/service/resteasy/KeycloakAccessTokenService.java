/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.resteasy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenServiceException;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.service.apache.HttpKeycloakAccessTokenService;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;
import org.apache.http.HttpHeaders;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * This class provides an implementation of AccessTokenService that rely on keycloak-admin-client library.
 * @see HttpKeycloakAccessTokenService for an implementation using directly Http requests.
 */
public class KeycloakAccessTokenService implements AccessTokenService {

    private final String keycloakUrl;
    private final String clientId;
    private final String clientSecret;
    public final String realmsProtocolOpenIdConnectTokenPath;
    public static final String CLIENT_CREDENTIALS = "client_credentials";
    public static final String INVALID_SCOPE_ERROR = "invalid_scope";
    public static final String INVALID_CLIENT_ERROR = "invalid_client";
    private static final String MACHINE_ROLE = "machine";
    private static final String ADMIN_ROLE = "role:gazelle_admin";
    private final ClientM2MRegistrationService clientM2MRegistrationService;

    public KeycloakAccessTokenService(ClientM2MRegistrationService clientM2MRegistrationService, SSOConfigurationProvider ssoConfigurationProvider) {
        this(clientM2MRegistrationService,
                ssoConfigurationProvider.getSSOUrl(),
                ssoConfigurationProvider.getSSORealm(),
                ssoConfigurationProvider.getSSOAdminUser(),
                ssoConfigurationProvider.getSSOAdminPassword(),
                ssoConfigurationProvider.getClientId(),
                ssoConfigurationProvider.getClientSecret());
    }

    public KeycloakAccessTokenService(ClientM2MRegistrationService clientM2MRegistrationService, String keycloakUrl, String keycloakRealm, String keycloakAdminUser, String keycloakAdminPassword, String clientId, String clientSecret) {
        this.clientM2MRegistrationService = clientM2MRegistrationService;
        this.keycloakUrl = keycloakUrl;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.realmsProtocolOpenIdConnectTokenPath = "/realms/" + keycloakRealm + "/protocol/openid-connect/token";
    }

    @Override
    public String getAccessToken() {
        return getAccessTokenWithRetry(null, true);
    }

    public String getAccessToken(String clientScopes) {
        if (clientScopes == null)
            throw new IllegalArgumentException("clientScopes is null");

        return getAccessTokenWithRetry(clientScopes, true);
    }

    private String getAccessTokenWithRetry(String clientScopes, boolean retryIfFailed) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost accessTokenRequest = generatePostRequest(clientScopes);

            try (CloseableHttpResponse response = httpClient.execute(accessTokenRequest)) {
                //if it failed because of bad scope and retryIfFailed is true, then retry after adding scopes
                if (shouldRetryRequest(retryIfFailed, response)) {
                    Set<String> roles = Set.of(MACHINE_ROLE, ADMIN_ROLE);
                    clientM2MRegistrationService.registerClient(clientId, clientSecret, roles);
                    return getAccessTokenWithRetry(clientScopes, false);
                } else if (response.getStatusLine().getStatusCode() >= 400)
                    throw new AccessTokenServiceException("Fail to get access token, HTTP error " + response.getStatusLine().getStatusCode());

                //if it's ok, then return the access token
                JsonNode jsonNode = getAccessTokenFromJson(response.getEntity().getContent());
                return jsonNode.asText();
            }
        } catch (IOException e) {
            throw new AccessTokenServiceException("Fail to get access token", e);
        }
    }

    private HttpPost generatePostRequest(String clientScope) throws UnsupportedEncodingException {
        HttpPost accessTokenRequest = new HttpPost(keycloakUrl + realmsProtocolOpenIdConnectTokenPath);
        accessTokenRequest.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
        List<BasicNameValuePair> parameters = new LinkedList<>(Arrays.asList(
                new BasicNameValuePair("grant_type", CLIENT_CREDENTIALS),
                new BasicNameValuePair("client_id", clientId),
                new BasicNameValuePair("client_secret", clientSecret)
        ));
        if (clientScope != null)
            parameters.add(new BasicNameValuePair("scope", clientScope));
        accessTokenRequest.setEntity(new UrlEncodedFormEntity(parameters));
        return accessTokenRequest;
    }

    private JsonNode getAccessTokenFromJson(InputStream responseBody) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(responseBody).get("access_token");
        } catch (IOException e) {
            throw new AccessTokenServiceException("Fail to get access token from json received", e);
        }
    }

    private boolean shouldRetryRequest(boolean retryIfFailed, CloseableHttpResponse response) throws IOException {
        return retryIfFailed
                && (response.getStatusLine().getStatusCode() >= 400)
                && isInvalidScopeOrClientError(response.getEntity().getContent());
    }

   private boolean isInvalidScopeOrClientError(InputStream response) throws IOException {
        String extractedError = extractErrorFromJson(response);
        return extractedError != null && (extractedError.equals(INVALID_SCOPE_ERROR) || extractedError.equals(INVALID_CLIENT_ERROR));
    }

    private String extractErrorFromJson(InputStream response) throws IOException {
        try (response) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("error").asText();
        }
    }
}
