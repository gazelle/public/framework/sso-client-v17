/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.AccessControlList;
import net.ihe.gazelle.security.domain.GazelleIdentity;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.wildfly.common.Assert.assertFalse;
import static org.wildfly.common.Assert.assertTrue;


class AccessControlListTest {

    @Test
    void testCopyConstructor() {
        AccessControlList acl = new AccessControlList()
                .setEditors(Set.of("editor1", "editor2"))
                .setOwners(Set.of("owner"))
                .setReaders(Set.of("reader1"))
                .setReadAccessKey("readAccessKey0")
                .setPublic(true);

        AccessControlList newAcl = new AccessControlList(acl);
        assertTrue(newAcl.isPublic());
        assertTrue(newAcl.getEditors().contains("editor2"));
        assertTrue(newAcl.getOwners().contains("owner"));
        assertTrue(newAcl.getReaders().contains("reader1"));
        assertEquals("readAccessKey0", newAcl.getReadAccessKey());
    }

    @Test
    void testSettersAndGetters() {
        AccessControlList acl = new AccessControlList()
                .setEditors(Set.of("editor1", "editor2"))
                .setOwners(Set.of("owner"))
                .setReaders(Set.of("reader1"))
                .setReadAccessKey("readAccessKey1")
                .setPublic(false);

        assertFalse(acl.isPublic());
        assertFalse(acl.getEditors().isEmpty());
        assertTrue(acl.getEditors().contains("editor1"));
        assertFalse(acl.getOwners().isEmpty());
        assertTrue(acl.getOwners().contains("owner"));
        assertTrue(acl.getReaders().contains("reader1"));
        assertEquals("readAccessKey1", acl.getReadAccessKey());
    }


    @Test
    void testWithIdentities() {
        AccessControlList acl = new AccessControlList()
                .setEditors(Set.of("tester"))
                .setOwners(Set.of("tester"))
                .setReaders(Set.of("tester"));

        GazelleIdentity gazelleIdentity = new BaseGazelleIdentity(new PrincipalTester("tester"));
        assertTrue(acl.isAmongsTheEditors(gazelleIdentity));
        assertTrue(acl.isAmongsTheOwners(gazelleIdentity));
        assertTrue(acl.isAmongsTheReaders(gazelleIdentity));
    }

    @Test
    void testVerifier() {
        EqualsVerifier.simple().forClass(AccessControlList.class).verify();
    }

    @Test
    void testToString() {
        AccessControlList acl = new AccessControlList()
                .setEditors(Set.of("editor"))
                .setOwners(Set.of("owner"))
                .setReaders(Set.of("reader"));

        String aclString = acl.toString();
        assertTrue(aclString.contains("editor"));
        assertTrue(aclString.contains("owner"));
        assertTrue(aclString.contains("reader"));

    }
}
