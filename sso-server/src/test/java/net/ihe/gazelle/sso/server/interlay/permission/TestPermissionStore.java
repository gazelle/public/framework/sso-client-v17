/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.permission;

import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.PermissionStore;

import java.util.Set;

public class TestPermissionStore implements PermissionStore {

    @Override
    public Set<Permission> getPermissions() {
        return Set.of(
                new Permission("object:read", (identity, context) ->
                     identity.hasGroup("role:gazelle_admin") || identity.hasGroup("keycloak_admin")
                )
        );
    }
}
