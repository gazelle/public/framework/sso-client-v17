package net.ihe.gazelle.m2m.client.mock;

import io.quarkus.test.Mock;
import io.smallrye.jwt.build.Jwt;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenServiceException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

@Mock
public class MockAccessTokenService implements AccessTokenService {

    @Override
    public String getAccessToken(String scope) {
        try {
            PrivateKey privateKey = getPrivateKey("src/test/resources/private_key.der");
            return Jwt.issuer("http://localhost:12345/realms/gazelle")
                    .sign(privateKey);
        } catch (Exception e) {
            throw new AccessTokenServiceException(e);
        }
    }

    @Override
    public String getAccessToken() {
        return getAccessToken(null);
    }

    private static PrivateKey getPrivateKey(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }
}
