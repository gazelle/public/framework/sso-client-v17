package net.ihe.gazelle.m2m.client.interlay.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to limit the number of request to get an access token. It will only get a new one if the old one
 * has expired.
 */
public class AccessTokenCacheService implements AccessTokenService {
    private final Logger log = LoggerFactory.getLogger(AccessTokenCacheService.class);
    private final AccessTokenService accessTokenService;
    private static final Map<String, String> cache = new HashMap<>();
    private final ObjectMapper mapper;

    public AccessTokenCacheService(AccessTokenService accessTokenService) {
        this.mapper = new ObjectMapper();
        this.accessTokenService = accessTokenService;
    }

    @Override
    public String getAccessToken() {
        String token = cache.get("noScope");

        if (needNewToken(token)) {
            token = accessTokenService.getAccessToken();
            cache.put("noScope", token);
        }
        return token;
    }

    @Override
    public String getAccessToken(String clientScope) {
        String token = cache.get(clientScope);

        if (needNewToken(token)) {
            token = accessTokenService.getAccessToken(clientScope);
            cache.put(clientScope, token);
        }
        return token;
    }

    private boolean needNewToken(String token) {
        if (token != null) {
            // Check if the token is expired
            long expirationTime = extractExpirationTimeFromToken(token);
            long currentTime = System.currentTimeMillis() / 1000;
            return expirationTime < currentTime;
        }
        return true;
    }

    private long extractExpirationTimeFromToken(String token) {
        // Decode the jwt in parameters and return the expiration time
        String[] splitString = token.split("\\.");
        String base64EncodedBody = splitString[1];
        String body = new String(Base64.getDecoder().decode(base64EncodedBody));

        // Get the expiration time
        try {
            return mapper.readTree(body).get("exp").asLong();
        } catch (JsonProcessingException e) {
            log.warn("Unable to extract expiration time from jwt", e);
            return 0;
        }
    }
}
