/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache.dto;

import java.util.*;

public abstract class ClientScopeDTO {

    private String name;
    private String description;
    private final String protocol;
    private Map<String, String> attributes = new HashMap<>();
    private List<ProtocolMapperDTO> protocolMappers = new ArrayList<>();

    public ClientScopeDTO(String protocol) {
        this.protocol = protocol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProtocol() {
        return protocol;
    }


    public Map<String, String> getAttributes() {
        return new HashMap<>(attributes);
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes != null ?
                new HashMap<>(attributes) :
                new HashMap<String, String>();
    }

    public void addAttribute(String attributeName, String attributeValue) {
        attributes.put(attributeName, attributeValue);
    }

    public List<ProtocolMapperDTO> getProtocolMappers() {
        return new ArrayList<>(protocolMappers);
    }

    public void setProtocolMappers(List<ProtocolMapperDTO> protocolMappers) {
        this.protocolMappers = protocolMappers != null ?
                new ArrayList<>(protocolMappers) :
                new ArrayList<ProtocolMapperDTO>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientScopeDTO that = (ClientScopeDTO) o;
        return Objects.equals(name, that.name)
                && Objects.equals(description, that.description)
                && Objects.equals(protocol, that.protocol)
                && Objects.equals(attributes, that.attributes)
                && Objects.equals(protocolMappers, that.protocolMappers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                name,
                description,
                protocol,
                attributes,
                protocolMappers
        );
    }
}
