/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.filter;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.quarkus.security.UnauthorizedException;
import jakarta.annotation.Priority;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import static jakarta.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class JWTWebServerFilter implements ContainerRequestFilter {

    Logger log = LoggerFactory.getLogger(JWTWebServerFilter.class);

    private final String rootTestBedUrl = ConfigProvider.getConfig().getValue("gzl.root.test.bed.url", String.class);

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        log.trace("JWTWebServerFilter.filter");
        ProtectedResource annotation = retrieveProtectedAnnotation();

        // If annotation found
        if (annotation != null) {
                JsonWebToken jwt = getJWT();
                // If no jwt provided, abort request
                if (jwt == null || jwt.getClaimNames() == null)
                    abortRequestWithUnauthorizedResponse(containerRequestContext);
                else {
                    try {
                        assertTokenIsValid(jwt);
                    } catch (UnauthorizedException e) {
                        abortRequestWithUnauthorizedResponse(containerRequestContext);
                    }
                }
        }
    }

    JsonWebToken getJWT() {
        return CDI.current().select(JsonWebToken.class).get();
    }

    private ProtectedResource retrieveProtectedAnnotation() {
        Method resourceMethod = resourceInfo.getResourceMethod();
        // Retrieve annotation to get scope from implementation
        ProtectedResource annotation = resourceMethod.getAnnotation(ProtectedResource.class);

        if (annotation == null) {
            // Retrieve annotation from interface
            Class<?>[] interfaces = resourceInfo.getResourceClass().getInterfaces();
            if (interfaces.length > 0) {
                Optional<Method> method = Arrays.stream(interfaces[0].getMethods())
                        .filter(m -> m.getName().equals(resourceMethod.getName())).findFirst();
                // TODO find a better way to catch the interface with annotation ?
                if (method.isPresent()) {
                    return method.get().getAnnotation(ProtectedResource.class);
                }
            }
        }
        return annotation;
    }

    /**
     * Abort request with unauthorized response
     * @param containerRequestContext request context
     */
    private void abortRequestWithUnauthorizedResponse(ContainerRequestContext containerRequestContext) {
        String message = "{ \"error\" : \"" + HttpResponseStatus.UNAUTHORIZED.reasonPhrase() + "\",  " +
                "\"code\" : "+ HttpResponseStatus.UNAUTHORIZED.code()+" }";
        Response response = Response.status(HttpResponseStatus.UNAUTHORIZED.code())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .entity(message).build();
        containerRequestContext.abortWith(response);
    }

    private void assertTokenIsValid(JsonWebToken jwt) {
        if (jwt.getAudience() == null || !jwt.getAudience().contains(rootTestBedUrl))
            throw new UnauthorizedException("Bad audience");

        long currentTimestampSeconds = System.currentTimeMillis() / 1000;
        if (jwt.getIssuedAtTime() > currentTimestampSeconds) {
            throw new UnauthorizedException("Bad issued at time");
        }
        if (currentTimestampSeconds > jwt.getExpirationTime()) {
            throw new UnauthorizedException("Token expired");
        }
    }
}
