package net.ihe.gazelle.security.application.acl;

import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.PermissionStore;

import java.util.Set;

public class DefaultACLPermissionStore implements PermissionStore {

   public static final String CREATE_ACTION = "create";
   public static final String READ_ACTION = "read";
   public static final String READ_SECURITY_ACTION = "read-security";
   public static final String UPDATE_ACTION = "update";
   public static final String UPDATE_SECURITY_ACTION = "update-security";
   public static final String DELETE_ACTION = "delete";
   private final String resourceName;

   public DefaultACLPermissionStore(String resourceName) {
      this.resourceName = resourceName;
   }

   @Override
   public Set<Permission> getPermissions() {
      return Set.of(
            new Permission(getPermissionName(CREATE_ACTION), new DefaultACLPolicies.CreatePolicy()),
            new Permission(getPermissionName(READ_ACTION), new DefaultACLPolicies.ReadPolicy()),
            new Permission(getPermissionName(UPDATE_ACTION), new DefaultACLPolicies.UpdatePolicy()),
            new Permission(getPermissionName(DELETE_ACTION), new DefaultACLPolicies.DeletePolicy()),
            new Permission(getPermissionName(READ_SECURITY_ACTION), new DefaultACLPolicies.ReadACLPolicy()),
            new Permission(getPermissionName(UPDATE_SECURITY_ACTION), new DefaultACLPolicies.UpdateACLPolicy())
      );
   }

   public String getPermissionName(String action) {
      return resourceName + ":" + action;
   }

}
