package net.ihe.gazelle.security.domain;

/**
 * Authentication service for clients that need to assess the identity and the permissions of the accessing entity
 */
public interface Authn {

   /**
    * Get the identity of the entity accessing the application.
    *
    * @return the identity of the accessing entity or an empty identity if the entity is not authenticated
    * ({@link GazelleIdentity#isAuthenticated()} will return false in that case).
    *
    * See GazelleIdentity
    */
   GazelleIdentity getIdentity();

}
