/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PoliciesTest {

    private final Policy falsePolicy = (identity, ctx) -> false;
    private final Policy truePolicy = (identity, ctx) -> true;

    @Test
    void testBasicPolicy() {
        assertTrue(Policies.or(falsePolicy, truePolicy).evaluate(null));
        assertFalse(Policies.and(falsePolicy, truePolicy).evaluate(null));
        assertTrue(Policies.and(truePolicy, truePolicy).evaluate(null));
    }

    @Test
    void testIdentityPolicy() {
        assertFalse(Policies.isLoggedIn().evaluate(BaseGazelleIdentity.unauthenticatedIdentity()));
        assertFalse(Policies.isAdmin().evaluate(BaseGazelleIdentity.unauthenticatedIdentity()));

        GazelleIdentity gazelleIdentityAdmin = new BaseGazelleIdentity(new PrincipalTester("tester"))
                .setGroups(Set.of("role:gazelle_admin"));
        assertTrue(Policies.isAdmin().evaluate(gazelleIdentityAdmin));
        assertTrue(Policies.isLoggedIn().evaluate(gazelleIdentityAdmin));

        GazelleIdentity gazelleIdentityNoGroups = new BaseGazelleIdentity(new PrincipalTester("tester"))
                .setGroups(Set.of());
        assertFalse(Policies.isAdmin().evaluate(gazelleIdentityNoGroups));
        assertFalse(Policies.isAdmin().evaluate(gazelleIdentityNoGroups));
    }
}
