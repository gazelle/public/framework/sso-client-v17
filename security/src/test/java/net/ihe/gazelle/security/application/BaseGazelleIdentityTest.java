package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BaseGazelleIdentityTest {

   @Test
   void testUnauthenticatedIdentity() {
      GazelleIdentity identity = BaseGazelleIdentity.unauthenticatedIdentity();
      assertFalse(identity.isAuthenticated());
      assertFalse(identity.hasGroup("group"));
      assertNull(identity.getId());
      assertNull(identity.getName());
      assertNull(identity.getPrincipal());
      assertNull(identity.getOrganizationId());
      assertNull(identity.getOrganizationGroup());
   }

   @Test
   void testSettersAndGetters() {
      BaseGazelleIdentity identity = new BaseGazelleIdentity(new PrincipalTester("tester"));
      identity.setName("name");
      identity.setGroups(Set.of("group1", "org:organization"));

      assertTrue(identity.isAuthenticated());
      assertEquals("name", identity.getName());
      assertEquals("tester", identity.getId());
      assertEquals("organization", identity.getOrganizationId());
      assertEquals("org:organization", identity.getOrganizationGroup());
      assertTrue(identity.hasGroup("group1"));
   }
}
