package net.ihe.gazelle.m2m.client.mock;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import net.ihe.gazelle.m2m.client.interlay.filter.M2MAuthMethod;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import java.io.Closeable;

@RegisterRestClient(baseUri = "http://localhost:12345")
public interface RestClientSecured {

    String SECURED = "/secured";
    String UNSECURED = "/unsecured";

    @GET
    @Path(SECURED)
    @M2MAuthMethod
    String getOnSecuredEndpoint();

    @GET
    @Path(UNSECURED)
    String getOnUnsecuredEndpoint();

}
