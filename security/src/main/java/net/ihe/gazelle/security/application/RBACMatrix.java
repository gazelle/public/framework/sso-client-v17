package net.ihe.gazelle.security.application;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class RBACMatrix {

   private final Map<String, String> groupMatrix = new HashMap<>();

   public RBACMatrix() {
      //FIXME rbac.json read (matrix persistence) must be in interlay
      Properties rbacProperties = new Properties();
      try {
         rbacProperties.load(this.getClass().getResourceAsStream("/rbac.properties"));
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
      rbacProperties.forEach((permissionName, groups) -> groupMatrix.put(((String) permissionName), ((String) groups)));
   }

   public Set<String> getAllowedGroups(String permissionName) {
      String formatedGroups = groupMatrix.get(permissionName);
      if (formatedGroups != null && !formatedGroups.isBlank()) {
         return Set.of(formatedGroups.split(","));
      } else {
         throw new IllegalStateException(
               String.format("Permission named '%s' does not exists in the RBAC matrix.", permissionName));
      }
   }
}
