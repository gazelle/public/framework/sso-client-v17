/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.mock;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.config.MetadataServiceProvider;
import net.ihe.gazelle.sso.server.interlay.ws.SSOProvider;

@RequestScoped
@SSOProvider
public class MockInterfaceProvider extends MetadataServiceProvider {

    @Inject
    MetadataServiceProvider metadataServiceProvider;

    @Override
    public Service getMetadata() {
        return metadataServiceProvider.getMetadata();
    }
}