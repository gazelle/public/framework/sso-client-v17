# Security

This module will help you to secure your application, by retrieving authentication information the user or the client 
(authentication) and restricting access to protected resources or protected actions (authorization).


<!-- TOC -->
* [Security](#security)
  * [Import](#import)
  * [Introduction](#introduction)
  * [Authentication](#authentication)
    * [What is it and how to use](#what-is-it-and-how-to-use)
    * [Uniformization](#uniformization-)
  * [Authorization](#authorization)
    * [Permissions](#permissions)
    * [Policies](#policies)
    * [Permission store](#permission-store)
    * [Authorization](#authorization-1)
    * [Access Control List](#access-control-list)
    * [Role Based Access Control](#role-based-access-control)
<!-- TOC -->


## Import

To use the security module, you need to add this to your pom:

```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>security</artifactId>
</dependency>
```

## Introduction

If you have an application that manage resources, it is possible that at one point you to limit the access of these 
resources. The reasons are multiple, maybe the resource is confidential, or maybe it is to provide more privacy. Whatever
the reason, the idea is still the same. 

First you need to identify who is trying to access your application, that is the **Authentication** part. 
After that you will need to verify the rights of the entity and what it can do in your application, that is the 
**Authorization** part.

## Authentication


### What is it and how to use


Accessing an application is not solely reserved to human users, other entity can also access it, like another application 
or a process. Because of that we need a way to keep the information of the entity, this what we call an **identity**.
But how do we get those information ? Through authentication. It will verify who is the entity and then provides
us an identity.

In this module, authentication is done with the [Authn](./src/main/java/net/ihe/gazelle/security/domain/Authn.java) 
interface. It will get the [GazelleIdentity](./src/main/java/net/ihe/gazelle/security/domain/GazelleIdentity.java) for 
the current entity accessing the app. Then with default Context Dependency Injection (CDI) implementation, the identity
is injectable in every class used in CDI context.

This what it looks like in a Context Dependency Injection (CDI) context.

>:warning: **WARNING:** Your controller must be request scoped. Otherwise, the users will share the same identity.

```java
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.security.domain.GazelleIdentity;

@RequestScoped
public class MyClass {

  private GazelleIdentity identity;

  @Inject
  public MyClass(GazelleIdentity identity) {
    this.identity = identity;
  }

}
```

Here we inject the GazelleIdentity in our class and it is ready to use. This injection is possible thanks to the
[AuthnCDIWrapper](./src/main/java/net/ihe/gazelle/security/interlay/AuthnCDIWrapper.java) class.

For non-CDI context, we will see it in the next part.


A `GazelleIdentity` represent an entity accessing the application. It contains basic information (id, name, email, organization, groups,...)
that can be used in your application. It also contains a `Principal` that is used to determine the entity is authenticated
or not. Check the official Java documentation for more information on the `Principal` class 
[https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/security/Principal.html](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/security/Principal.html).

We provide a default implementation, `BaseGazelleIdentity` accessible [here](./src/main/java/net/ihe/gazelle/security/application/BaseGazelleIdentity.java),
that should cover most of the use cases. However, you can still extend it or create a new implementation of `GazelleIdentity`
should you need it.

### Uniformization 

The fact multiple types of entities access our application can mean multiple ways of authenticating and multiple types 
of identity. This can be a hassle to maintain. That is why this library aims to provide an easy way to uniformize the
different authentication methods and identities.

For that we use `Authenticators`, basically their goal is to provide a `GazelleIdentity` based on a type of authentication.

The default implementation of the `Authn` interface, needs a `GazelleIdentity` whose role is to provide a list of
`Authenticator`. Then, when a `GazelleIdentity` is needed, the default `Authn` implementation will cycle through all
`Authenticators` until one returns a `GazelleIdentity`. 

This library provides a Service Provider Interface Implementation (SPI) of `AuthenticatorProvider` that will load all
implementations of `Authenticator`. This allows for more extensibility as if you have a custom authentication method,
you can implement your own authenticator and add its fully qualified name in 
_/main/resources/META-INF/services/net.ihe.gazelle.security.application.Authenticator_. After that it will be available
for the service loader of Java. 

For more information on how SPI works, check this documentation [https://www.baeldung.com/java-spi](https://www.baeldung.com/java-spi).



## Authorization

Now we know who is accessing our application but how can we restrict their actions ? For that we need to grant the right
to do these actions. This is what we call a **permission**. This permission relies on statement that assert 
that it was granted. These statements are what we call **policies**.

Here is quick example on how the authorization check can be performed with this library:

```java
import net.ihe.gazelle.security.domain.Authz;
import net.ihe.gazelle.security.domain.GazelleIdentity;

private Authz authz;

public String getDocumentById(GazelleIdentity identity, String id) {
  //check for null values
  authz.assertAuthorized(identity, "read-document");
}
```

### Permissions

We need to specify permissions. A permission will grant the right or not to perform an action.
A permission is made of two elements:
- A **unique** name that represent the name of an action or of a controlled access
- A policy that will evaluate if the permission can be granted (more detail in [Policies](#policies))

Let's see an example, you have a document from your company that you want to share to your colleague. Some of them can 
edit it but the others can only read it. With we can create at least two permissions:
- One that will grant the right to read document based on some condition (policy)
- One that will grant the right to edit document based on some condition (policy)
 
In terms of code the use of the [Permission](./src/main/java/net/ihe/gazelle/security/application/Permission.java) class
will look like that:

```java
import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.Policy;

private Policy policyToRead;
private Policy policyToEdit;

Permission readDocument = new Permission("read-document", policyToRead);
Permission editDocument = new Permission("edit-document", policyToEdit);
```

As you can see, we have created two permission to control actions done on a document.

A permission has two methods:
- One to retrieve the name of the permission
- One to check if the permission is granted based on the identity of a user and a context. This context is optional and can be anything
needed by the permission to grant access. It can be the id of an organization, a user, a list of object, etc. This context
will be useful for the [policies](#policies)

Now we need to decide who can read and who can edit the document, and for that we have the policies.


### Policies

A policy is statement that when evaluated will tend to authorize or not the access. We will see after why it only tends 
to authorization and not directly authorize access. Before that let see an example of a what a policy can be.

Let's suppose that in the company you are, there are multiple types of career and one of them is software development.
With this information we create two policies:
- The person is from the same company
- The person is a software developer

For that we use the interface [Policy](./src/main/java/net/ihe/gazelle/security/application/Policy.java). This interface
expose this one method: ```boolean evaluate(GazelleIdentity identity, Object... context)``` that we will break down for 
better understanding.

First we need a ```GazelleIdentity``` which, as we have seen before, represent the entity trying to access the application.
Then we have the context. The is optional and can be one or more objects required for the evaluation of the policy. The 
context can be anything: a user id, a list of groups, etc.
Finally, the evaluation of the policy will return either true or false.


>:warning: **Warning**: Keep in mind that a policy can be composed with other and a positive evaluation will tend to the
> authorized access and the result can be used differently depending on the Permission that use it.

You two ways of implementing a Policy. First you can implement it like you would do for any interface and create a new
class like this:

```java
import net.ihe.gazelle.security.application.Policy;
import net.ihe.gazelle.security.domain.GazelleIdentity;
import net.ihe.gazelle.security.domain.MissingContextException;

public class isInSameCompanyPolicy implements Policy {

  @Override
  public boolean evaluate(GazelleIdentity identity, Object... context) {
    if (identity == null)
      throw new IllegalArgumentException("Identity should not be null");
    //If context is required, you need to check its existence
    if (context == null)
      throw new MissingContextException("Context is missing");
    
    return identity.getCompany().equals(context[0]);
  }
}
```

Or you can do it with lambda functions, like this

```java
import net.ihe.gazelle.security.application.Policy;

private static Policy isInSameCompany(){
    (identity, context) -> {
      if (identity == null)
        throw new IllegalArgumentException("Identity should not be null");
      if (context == null)
        throw new MissingContextException("Context is missing");
      
      return identity.getCompany().equals(context[0]);
    };
}

private static Policy isSoftwareDeveloper(){
    (identity, context) -> {
      if (identity == null)
        throw new IllegalArgumentException("Identity should not be null");
      
      return identity.getCareer().equals("software-developer");
    };
}
```

In our example, the methode `isInSameCompany()` require a company to know if the identity is in the same company.

As you can see, those policies are only statements and do not grant access alone. For that they need to be paired with 
permissions. In our example from before, we had to permissions, read-document and edit-document. Imagine we say that to 
read the document, a person must be in the same company and to edit a user must be a software developer. 

In terms of code it will look like that:


```java
import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.Policy;

private Policy policyToRead;
private Policy policyToEdit;

Permission readDocument = new Permission("read-document", isInSameCompany());
Permission editDocument = new Permission("edit-document", isSoftwareDeveloper());
```

What if we want to have more than one policy to grant a permission ? Do we update the current policy ?
No, we create a new one, and we will combine them. In the previous example only user from the same company can read the document but any
software developer can edit it no matter their company. This is security issue in our case. Let's update the permission 
to edit document. The permission to edit is granted when a user is a software developer **and** the user is from the same
company. As you can see it combines both the policies we create earlier.

To this, the framework provides a set of utilities policies such as:
- **AND** policy 
- **OR** policy

They can be found in the [Policies](./src/main/java/net/ihe/gazelle/security/application/Policies.java) class.

In terms of code it will look like that:


```java
import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.Policies;
import net.ihe.gazelle.security.application.Policy;

private Policy policyToRead;
private Policy policyToEdit;

Permission readDocument = new Permission("read-document", isInSameCompany());
Permission editDocument = new Permission("edit-document", Policies.and(isInSameCompany(), isSoftwareDeveloper()));
```

Remember earlier when we said that policies tend to authorization as opposed to directly granting access, the fact that
we can combine them is why.


### Permission store

Now that we know how to create permissions, we can use them, right ?

The thing is, we do not work with the permissions directly, they first need to be stored somewhere in a 
[permission store](./src/main/java/net/ihe/gazelle/security/application/PermissionStore.java).

If we keep our example from before, let's put our permission in an implementation of the permission store :

```java
import net.ihe.gazelle.security.application.Permission;
import net.ihe.gazelle.security.application.PermissionStore;
import net.ihe.gazelle.security.application.Policies;
import net.ihe.gazelle.security.application.Policy;

public class DocumentPermissionStore implements PermissionStore {

  @Override
  public Set<Permission> getPermissions() {
    return Set.of(new Permission("read-document", isInSameCompany()),
            new Permission("edit-document", Policies.and(isInSameCompany(), isSoftwareDeveloper()))
    );
  }

  private static Policy isInSameCompany() {
    (identity, context) -> identity != null && identity.getCompany().equals(context[0]);
  }

  private static Policy isSoftwareDeveloper() {
    (identity, context) -> identity != null && identity.getCareer().equals("software-developer");
  }
}
```

Now we just need to add the fully qualified name of our permission store and add it in the 
*main/resources/META-INF/services/net.ihe.gazelle.security.application.PermissionStore* file.

The default implementation of the framework will load all permission stores declared and automatically use them for 
authorization. It allows better maintainability and extensibility. It means that if in your project you have a library 
that comes with its own implementation, it will load the one from your project and the one from the library.


### Authorization

Now that we have our permissions stored and accessible, we can now assert that the identity accessing the resource is
authorized to do so.

For that, one last class is needed, the [Authz class](./src/main/java/net/ihe/gazelle/security/domain/Authz.java).

This interface gives us three useful methods:
- `boolean isAuthorized(identity, action, context...)` that will grant the permission or not and will return a boolean. 
Additional context may be required for a given permission
- `void assertAuthorized(identity, action, context...)` is similar to isAuthorized but this one will throw an exception 
if the permission is not granted. Additional context may be required for a given permission
- `C filterOutUnauthorized(identity, action, C collection)` that given a collection will remove each element where the 
permission could not be granted.  


Finally, if we were to put these concepts in practice and keeping the same example as before, here is what it could look 
like in terms of code:

```java
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.security.domain.Authz;
import net.ihe.gazelle.security.domain.GazelleIdentity;

@RequestScoped
public class DocumentService {
  @Inject
  Authz authz;

  public Document getDocumentById(GazelleIdentity identity, String id) {
    //null check
    //before doing any logic we check if the current identity is authorized
    Document documentToReturn = dao.getDocument(id);
    //Here we need the company for the context
    authz.assertAuthorized(identity, "read-document", documentToReturn.getCompany());
    //do something
  }

  public Document editDocumentById(GazelleIdentity identity, String id, Document newContent) {
    //null check
    //before doing any logic we check if the current identity is authorized
    Document documentToReturn = dao.getDocument(id);
    //Here we need the company for the context
    authz.assertAuthorized(identity, "edit-document", documentToReturn.getCompany());
    //do something
  }
}
```

>:information_source: **Note**: Even though the ```DocumentPermissionStore``` is not explicitly used in the example, the 
> default implementation of Authz will load it with SPI technique.

For better visualization on how the authorization and authentication can work in a Rest controller, check this sequence
diagram:

![authz-diagram](../doc/assets/auth-sequence.png)


Everything we've learned so far can be enough to manage access and permissions. But if we want to implement one of the 
two patterns of access management Role Based Access Control (RBAC) or Access Control List (ACL), the framework provides
everything for it with almost no code for the developer.


### Access Control List


The Access Control List or ACL is a type of permission management where it is an object that knows who can access it. It
means that each resource contains a list of permissions that allow a user to do some actions on it.

For example, you have an image from your holidays abroad. You want to share this image to some colleagues but not to all the
company. You do not allow any edition of the picture. So what you will do is add an
[AccessControlList](./src/main/java/net/ihe/gazelle/security/domain/AccessControlList.java) to the object containing the 
picture and you will:
- add yourself as the owner
- add your colleagues, let's say Bob and Alice, as readers
- and let the list of editor empty

Now the access to your picture is limited to only you, Bob and Alice.


To do that a resource first need to implement the ```ProtectedResource``` interface.

```java
import net.ihe.gazelle.security.domain.AccessControlList;
import net.ihe.gazelle.security.domain.ProtectedResource;

public class Document implements ProtectedResource {
  private String name;
  private final AccessControlList accessControlList;

  public Item() {
    this.accessControlList = new AccessControlList();
  }
  
  public String getId() {
    return id;
  }

  @Override
  public AccessControlList getAccessControlList() {
    return accessControlList;
  }
}
```

The ```AccessControlList``` comes with multiple elements to fine tune the access of the resource:
- 3 groups: owners, editors and readers.
- an attribute ```isPublic``` for document accessible to all
- an attribute ```readAccessKey``` for document that require private key access.

Then you need a permission store. The framework provides a default one, ```DefaultACLPermissionStore```, that requires 
the name of the resources to be used. You can use it like this:

```java
public class MyApplicationPermissionStore implements PermissionStore {

   @Override
   public Set<Permission> getPermissions() {
      return new DefaultACLPermissionStore("document").getPermissions();
   }
}
```
The ```DefaultACLPermissionStore``` will automatically generate permissions in the form of 
```<resource-name-given>:<action>```. In our example for the "document" resource we will have these permissions:
- ```document:create``` --> By default everyone is able to create
- ```document:read``` --> only those in owners, in editors or in readers groups, and also administrators can read 
- ```document:update``` --> only those in owners or in editors groups and administrators can update
- ```document:delete``` --> only those in owners group and administrators can delete
- ```document:read-security``` --> only those in owners group and administrators can read
- ```document:update-security``` --> only those in owners group and administrators can update

### Role Based Access Control

>:warning: **Warning**: This feature is experimental and not fully implemented.

The Role Based Access Control or RBAC is type of permission management based on, you guessed it, roles ! That means that
roles are given a set of permissions. Then this role is given to a user that will give them privileges.

RBAC is only a constraint on whether you belong to a certain group. So it is very useful to restrict access to actions,
but not to restrict resource access, as you cannot give context to this permission scheme.

For example, you have an application that manage documents. For each a document, you can perform a set of action like read,
update or delete. You want to limit who can perform each action. For that you create multiple roles:
- Reader -> only allowed to read the document
- Maintainer -> allowed to read and to edit the document
- Admin -> allowed to read, edit and delete


>:information_source: **Note**: In Gazelle we do not have roles, everything is a group.

Now you need to assign the group corresponding to their authorized access to each user.

You create a permission store as we have seen before. In this you create permissions and some of them can rely on a
```RBACPolicy```. This RBACPolicy will call a ```RBACMatrix``` that will verify if the permission is granted for the 
specified role.

The ```RBACMatrix``` will parse a property file, _rbac.properties_ located in  _main/resources/_, containing all the
permissions granted for the groups in the following format:

```properties
permission_name:group1,group2
```


>:warning: **Warning**: As said before this part is experimental and the provided implementation may work or may have 
> issues as the matrix is stored in a file (low-level), it causes difficulties for dependency inversion principle.



