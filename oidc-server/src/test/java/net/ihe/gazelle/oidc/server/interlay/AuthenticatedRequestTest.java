package net.ihe.gazelle.oidc.server.interlay;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import org.apache.http.HttpHeaders;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Test;

import static net.ihe.gazelle.oidc.server.OIDCJWTGenerator.*;
import static net.ihe.gazelle.sso.server.interlay.mock.MockRestServer.PROTECTED_ENDPOINT;
import static net.ihe.gazelle.sso.server.interlay.mock.MockRestServer.UNPROTECTED_ENDPOINT;

@QuarkusTest
class AuthenticatedRequestTest {

    public static final String BEARER = "Bearer ";

    @Test
    void testCorrectRequest() {
        String validJwt = getValidJwt();
        RestAssured.given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, BEARER + validJwt)
                .get(PROTECTED_ENDPOINT)
                .then()
                .statusCode(200);
    }

    @Test
    void testWithNoAuthorizationHeader() {
        RestAssured.given()
            .when()
            .get(PROTECTED_ENDPOINT)
            .then()
            .statusCode(401);
    }

    @Test
    void testWithBadToken() { testRequestWithUnauthenticatedResponse("badToken"); }

    @Test
    void testWithBadBearerToken() {
        testRequestWithUnauthenticatedResponse("Bearer badToken");
    }

    @Test
    void testWithExpiredToken() {
        String expiredToken = getExpiredJwt();
        testRequestWithUnauthenticatedResponse(BEARER + expiredToken);
    }

    @Test
    void testWithBadAudienceToken() {
        String validJwt = getValidJwtWithAudience("bad-audience");
        testRequestWithUnauthenticatedResponse(BEARER + validJwt);
    }

    @Test
    void testBadIssuedAt() {
        String validJwt = getBadIssuedAtToken();
        testRequestWithUnauthenticatedResponse(BEARER + validJwt);
    }

    @Test
    void testWithBadIssuerToken() {
        String validJwt = getValidJwtWithIssuer("bad-issuer");
        testRequestWithUnauthenticatedResponse(BEARER + validJwt);
    }

    @Test
    void testUnprotectedResourceWithoutToken() {
        testRequestOnUnAuthorizedEndpoint(null);
    }

    @Test
    void testUnprotectedResourceWithToken() {
        String jwt = getValidJwt();
        testRequestOnUnAuthorizedEndpoint(BEARER + jwt);
    }

    private void testRequestWithUnauthenticatedResponse(String headerValue) {
        RestAssured.given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, headerValue)
                .get(PROTECTED_ENDPOINT)
                .then()
                .statusCode(RestResponse.StatusCode.UNAUTHORIZED);
    }

    private void testRequestOnUnAuthorizedEndpoint(String headerValue) {
        if (headerValue == null) {
            RestAssured.given()
                    .when()
                    .get(UNPROTECTED_ENDPOINT)
                    .then()
                    .statusCode(RestResponse.StatusCode.OK);
        } else {
            RestAssured.given()
                    .when()
                    .header(HttpHeaders.AUTHORIZATION, headerValue)
                    .get(UNPROTECTED_ENDPOINT)
                    .then()
                    .statusCode(RestResponse.StatusCode.OK);
        }
    }
}
