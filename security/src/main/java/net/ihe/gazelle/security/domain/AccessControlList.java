package net.ihe.gazelle.security.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class AccessControlList {

   private Set<String> owners = new HashSet<>();
   private Set<String> readers = new HashSet<>();
   private Set<String> editors = new HashSet<>();
   private boolean isPublic = false;
   private String readAccessKey ;

   public AccessControlList(){
   }
   public AccessControlList(AccessControlList accessControlList) {
      this.owners = new HashSet<>(accessControlList.owners);
      this.readers = new HashSet<>(accessControlList.readers);
      this.editors = new HashSet<>(accessControlList.editors);
      this.isPublic = accessControlList.isPublic;
      this.readAccessKey = accessControlList.readAccessKey;
   }

   public Set<String> getOwners() {
      return  new HashSet<>(owners);
   }

   public AccessControlList setOwners(Set<String> owners) {
      this.owners = owners != null ? owners : new HashSet<>();
      return this;
   }

   public Set<String> getReaders() {
      return new HashSet<>(readers);
   }

   public AccessControlList setReaders(Set<String> readers) {
      this.readers = readers != null ? readers : new HashSet<>();
      return this;
   }

   public Set<String> getEditors() {
      return new HashSet<>(editors);
   }

   public AccessControlList setEditors(Set<String> editors) {
      this.editors = editors != null ? editors : new HashSet<>();
      return this;
   }

   public boolean isPublic() {
      return isPublic;
   }

   public AccessControlList setPublic(boolean isPublic) {
      this.isPublic = isPublic;
      return this;
   }

   public String getReadAccessKey() {
      return readAccessKey;
   }

   public AccessControlList setReadAccessKey(String readAccessKey) {
      this.readAccessKey = readAccessKey;
      return this;
   }

   //TODO ceoche: Should those following methods be in Policy or this POJO ? I am not sure, but it seems to be more
   // about policy decision to me rather than information.
   public boolean isAmongsTheOwners(GazelleIdentity identity) {
      return isInGroup(getOwners(), identity);
   }

   public boolean isAmongsTheEditors(GazelleIdentity identity) {
      return isInGroup(getEditors(), identity);
   }

   public boolean isAmongsTheReaders(GazelleIdentity identity) {
      return isInGroup(getReaders(), identity);
   }

   private boolean isInGroup(Set<String> groupMembers, GazelleIdentity identity) {
      for (String member : groupMembers) {
         if (identity.getId().equals(member) || identity.getGroups().contains(member)) {
            return true;
         }
      }
      return false;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof AccessControlList that)) {
         return false;
      }
      return isPublic == that.isPublic && Objects.equals(owners, that.owners) && Objects.equals(
            readers, that.readers) && Objects.equals(editors, that.editors) && Objects.equals(
            readAccessKey, that.readAccessKey);
   }

   @Override
   public int hashCode() {
      return Objects.hash(owners, readers, editors, isPublic, readAccessKey);
   }

   @Override
   public String toString() {
      return "AccessControlList{" +
            "owners=" + owners +
            ", readers=" + readers +
            ", editors=" + editors +
            ", isPublic=" + isPublic +
            ", readAccessKey='" + readAccessKey + '\'' +
            '}';
   }
}
