package net.ihe.gazelle.security.domain;

import java.security.Principal;
import java.util.Set;

/**
 * Identity represent an entity (user, application or process) accessing the application.
 */
public interface GazelleIdentity {

   /**
    * Get the unique id of the entity if authenticated. Calls {@link #isAuthenticated()} first to check.
    *
    * @return the id of the entity or null if not authenticated.
    */
   String getId();

   /**
    * Get the human-readable name of the entity if authenticated. Calls {@link #isAuthenticated()} first to check.
    *
    * @return the name of the entity or null if not authenticated.
    */
   String getName();

   /**
    * Get the groups to which belongs that entity if authenticated.
    *
    * @return A set of entity's group names or an empty set if no groups or not authenticated.
    */
   Set<String> getGroups();

   /**
    * Get the organization groups to which belons the entity if authenticated.
    *
    * @return A set of entity's organization group names or an empty set.
    */
   String getOrganizationGroup();

   /**
    * Get the organization ids to which belongs the entity if authenticated.
    *
    * @return A set the entity's organization ids or an empty set.
    */
   String getOrganizationId();

   /**
    * Get the principal used to identify the entity if authenticated.
    *
    * @return the principal or null if not authenticated.
    */
   Principal getPrincipal();

   /**
    * Is this identity representing an authenticated entity? It is best to call that method before any other method to
    * avoid getting null values.
    *
    * @return true if authenticated, false otherwise.
    */
   boolean isAuthenticated();

   /**
    * Does this entity belongs to the given groups?
    *
    * @param groupName The group name to test for.
    * @return true if the entity is authenticated and belong to that group, false otherwise.
    */
   boolean hasGroup(String groupName);
}
