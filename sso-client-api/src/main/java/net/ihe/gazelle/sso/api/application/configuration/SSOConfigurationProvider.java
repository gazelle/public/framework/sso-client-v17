/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.application.configuration;

public interface SSOConfigurationProvider {

    /**
     * Get the url of the SSO server
     * @return the SSO server url
     */
    String getSSOUrl();

    /**
     * Get the name of the realm the SSO server uses
     * @return the realm name
     */
    String getSSORealm();

    /**
     * Get the username of the user that can perform admin operations
     * @return the username
     */
    String getSSOAdminUser();

    /**
     * Get the password of the user that can perform admin operations
     * @return the password
     */
    String getSSOAdminPassword();

    String getK8sId();

    String getServiceName();

    /**
     * Get the clientId for the current application
     * @return the clientId
     */
    default String getClientId(){
        String m2mPrefix = "m2m-";
        String k8sid = getK8sId();
        String serviceName = getServiceName();
        // If k8sId is set, use it to compute clientId
        if (k8sid != null && !k8sid.isBlank()) {
            return m2mPrefix +  k8sid.substring(0, k8sid.lastIndexOf("-"));
            // If serviceName is set, use it to compute clientId
        } else if (serviceName != null && !serviceName.isBlank()) {
            return m2mPrefix + serviceName;
        } else {
            throw new IllegalStateException("Either k8sId or service name variables/configurations are missing");
        }
    }

    /**
     * Get the secret for the client of the current application
     * @return the secret
     */
    String getClientSecret();
}
