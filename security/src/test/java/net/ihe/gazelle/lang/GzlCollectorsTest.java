/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.lang;

import org.junit.jupiter.api.Test;

import java.util.stream.Collector;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GzlCollectorsTest {

    private static class EmptyException extends RuntimeException {}
    private static class TooManyException extends RuntimeException {}

    Collector<Object, ?, Object> gzlCollectors = GzlCollectors.toSingletonOrThrow(TooManyException::new, EmptyException::new);

    @Test
    void testToSingletonOrThrow() {
        Stream<Object> stream1 = Stream.empty();
        assertThrows(EmptyException.class, () -> stream1.collect(gzlCollectors));

        Stream<Object> stream2 = Stream.of("test", "test2");
        assertThrows(TooManyException.class, () -> stream2.collect(gzlCollectors));
    }

    @Test
    void testToSingletonOK() {
        assertDoesNotThrow(() -> Stream.of("test").collect(gzlCollectors));
        assertDoesNotThrow(() -> Stream.of("").collect(gzlCollectors));
    }
}
