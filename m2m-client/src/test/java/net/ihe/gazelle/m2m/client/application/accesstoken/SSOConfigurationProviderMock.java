package net.ihe.gazelle.m2m.client.application.accesstoken;

import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;

public class SSOConfigurationProviderMock implements SSOConfigurationProvider {
    private String clientSecret = "clientSecret";
    public String keycloakUrl = "http://localhost:12345";
    public String keycloakRealm = "gazelle";
    public String adminUser = "admin";
    public String adminPassword = "test";
    public String gumK8sId = "gazelle-user-management-12345-45678";
    public String gum = "gazelle-user-management";

    public SSOConfigurationProviderMock() {
    }

    @Override
    public String getSSOUrl() {
        return keycloakUrl;
    }

    @Override
    public String getSSORealm() {
        return keycloakRealm;
    }

    @Override
    public String getSSOAdminUser() {
        return adminUser;
    }

    @Override
    public String getSSOAdminPassword() {
        return adminPassword;
    }

    @Override
    public String getK8sId() {
        return gumK8sId;
    }

    @Override
    public String getServiceName() {
        return gum;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    public SSOConfigurationProviderMock setSSOUrl(String url) {
        keycloakUrl = url;
        return this;
    }

    public SSOConfigurationProviderMock setSSORealm(String realm) {
        keycloakRealm = realm;
        return this;
    }

    public SSOConfigurationProviderMock setSSOAdminUser(String user) {
        adminUser = user;
        return this;
    }

    public SSOConfigurationProviderMock setSSOAdminPassword(String password) {
        adminPassword = password;
        return this;
    }

    public SSOConfigurationProviderMock setK8sId(String id) {
        gumK8sId = id;
        return this;
    }
}
