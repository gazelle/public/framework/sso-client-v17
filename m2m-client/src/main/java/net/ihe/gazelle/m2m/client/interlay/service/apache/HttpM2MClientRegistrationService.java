/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache;

import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class HttpM2MClientRegistrationService implements ClientM2MRegistrationService {
    Logger log = LoggerFactory.getLogger(HttpM2MClientRegistrationService.class);
    public static final String MACHINE_ROLE = "machine";
    public static final String ADMIN_ROLE = "admin_role";
    private final M2MClientDAO m2MClientDAO;

    public HttpM2MClientRegistrationService(M2MClientDAO m2MClientDAO) {
        this.m2MClientDAO = m2MClientDAO;
    }

    @Override
    public void registerClient(String clientId, String clientSecret, Set<String> roles) {
        log.info("Registering M2M client in Keycloak (apache): {}", clientId);
        createOrUpdateClient(clientId, roles);
    }
    private void createOrUpdateClient(String clientId, Set<String> roles) {

        String idOfClient = m2MClientDAO.getIdOfClient(clientId);
        if (idOfClient != null && !idOfClient.isEmpty()) {
            m2MClientDAO.updateClient(idOfClient, clientId);

            String microprofileClientScopeId = m2MClientDAO.getClientScopeId("microprofile-jwt");
            m2MClientDAO.addClientScopeToClient(idOfClient,microprofileClientScopeId,false);

            String m2meClientScopeId = m2MClientDAO.getClientScopeId("m2m-client-scope");
            m2MClientDAO.addClientScopeToClient(idOfClient,m2meClientScopeId,false);
        } else {
            m2MClientDAO.createClient(clientId);
            idOfClient = m2MClientDAO.getIdOfClient(clientId);
        }
        for (String role: roles){
            String roleId = m2MClientDAO.getRealmRoleId(role);
            m2MClientDAO.grantRoleToClient(idOfClient,roleId, role);
        }
    }
}
