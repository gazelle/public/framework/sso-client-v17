package net.ihe.gazelle.m2m.client.interlay.factory;

import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.dao.M2MClientDAOImpl;
import net.ihe.gazelle.m2m.client.interlay.service.AccessTokenCacheService;
import net.ihe.gazelle.m2m.client.interlay.service.apache.HttpM2MClientRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.service.resteasy.KeycloakAccessTokenService;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;


public class M2MServiceQuarkusFactory {

    private final SSOConfigurationProvider ssoConfigurationProvider;

    @Inject
    public M2MServiceQuarkusFactory(SSOConfigurationProvider ssoConfigurationProvider) {
        this.ssoConfigurationProvider = ssoConfigurationProvider;
    }

    @Produces
    public ClientM2MRegistrationService getClientM2MClientRegistrationService() {
            return new HttpM2MClientRegistrationService(new M2MClientDAOImpl(ssoConfigurationProvider));
    }

    @Produces
    public AccessTokenService getAccessTokenService() {
        AccessTokenService accessTokenService = new KeycloakAccessTokenService(
                getClientM2MClientRegistrationService(),
                ssoConfigurationProvider);
        // Wrap the service in a cache
        return new AccessTokenCacheService(accessTokenService);
    }
}
