/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.oidc.server;

import io.smallrye.jwt.build.Jwt;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.List;

import static net.ihe.gazelle.sso.api.domain.SSOConstProvider.*;

public class OIDCJWTGenerator {

    private static final PrivateKey privateKey;
    public static final String AUDIENCE_CLAIM_VALUE = "http://localhost";
    public static final String AUTHN_METHOD_CLAIM_VALUE = "OIDC";
    public static final List<String> GROUPS_CLAIM_VALUE = List.of("org:kereval", "role:gazelle_admin");
    public static final String FIRSTNAME_CLAIM_VALUE = "myFirstname";
    public static final String LASTNAME_CLAIM_VALUE = "myLastname";
    public static final String EMAIL_CLAIM_VALUE = "myEmail";
    public static final String ISSUER_CLAIM_VALUE = "https://localhost:8443";
    public static final String UPN_CLAIM_VALUE = "admin";

    static {
        try {
            privateKey = getPrivateKey("src/test/resources/private_key.der");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getJwt(String upn, String audience, String issuer, String firstName,
                                String lastName, String email, String authnMethod,
                                List<String> groups, long issuedAt, long expirationTime) {
        return Jwt.upn(upn)
                .claim("azp", "oidc-test")
                .claim(CLAIM_AUTHN_METHOD, authnMethod)
                .claim(CLAIM_GROUPS, groups)
                .claim(CLAIM_FIRSTNAME, firstName)
                .claim(CLAIM_LASTNAME, lastName)
                .claim(CLAIM_EMAIL, email)
                .issuedAt(issuedAt)
                .audience(audience)
                .issuer(issuer)
                .expiresAt(expirationTime)
                .sign(privateKey);
    }

    public static String getJwt(String upn, String audience, String issuer, String firstName,
                                String lastName, String email, String authzMethod,
                                List<String> groups) {
        long issuedAt = System.currentTimeMillis() / 1000;
        long expirationTime = issuedAt + 60;
        return getJwt(upn, audience, issuer, firstName, lastName, email, authzMethod, groups, issuedAt, expirationTime);
    }

    public static String getValidJwt() {
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE, GROUPS_CLAIM_VALUE);
    }

    public static String getValidJwtWithIssuer(String issuer) {
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,issuer,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE, GROUPS_CLAIM_VALUE);
    }

    public static String getValidJwtWithAuthzMethod(String authzMethod) {
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE,authzMethod, GROUPS_CLAIM_VALUE);
    }

    public static String getValidJwtWithAudience(String audience) {
        return getJwt(UPN_CLAIM_VALUE,audience,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE, GROUPS_CLAIM_VALUE);
    }

    public static String getValidJwtWithGroups(List<String> groups) {
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE,groups);
    }

    public static String getValidJwtWithIdAndGroups(String upn, List<String> groups) {
        return getJwt(upn,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE,groups);
    }

    public static String getExpiredJwt() {
        long issuedAt = System.currentTimeMillis() / 1000;
        long badExpirationTime = issuedAt - 60;
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE, GROUPS_CLAIM_VALUE,issuedAt,badExpirationTime);
    }

    public static String getBadIssuedAtToken() {
        long badIssuedTime = System.currentTimeMillis() / 1000 + 60;
        long expirationTime = System.currentTimeMillis() / 1000 + 60;
        return getJwt(UPN_CLAIM_VALUE,AUDIENCE_CLAIM_VALUE,ISSUER_CLAIM_VALUE,FIRSTNAME_CLAIM_VALUE,LASTNAME_CLAIM_VALUE,EMAIL_CLAIM_VALUE, AUTHN_METHOD_CLAIM_VALUE, GROUPS_CLAIM_VALUE,badIssuedTime,expirationTime);
    }


    private static PrivateKey getPrivateKey(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }
}
