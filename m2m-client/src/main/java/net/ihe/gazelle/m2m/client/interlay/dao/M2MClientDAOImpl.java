/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.dao;

import net.ihe.gazelle.m2m.client.interlay.service.apache.KeycloakHttpClient;
import net.ihe.gazelle.m2m.client.interlay.service.apache.KeycloakHttpClientException;
import net.ihe.gazelle.m2m.client.interlay.service.apache.M2MClientDAO;
import net.ihe.gazelle.m2m.client.interlay.service.apache.M2MClientDAOException;
import net.ihe.gazelle.m2m.client.interlay.service.apache.dto.M2MKeycloakDTOMapper;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static net.ihe.gazelle.m2m.client.interlay.service.apache.KeycloakHttpClient.*;


public class M2MClientDAOImpl implements M2MClientDAO {

    private static final Logger LOG = LoggerFactory.getLogger(M2MClientDAOImpl.class);
    public static final String CLIENTS = "/clients/";

    private final KeycloakHttpClient keycloakHttpClient;
    private final M2MKeycloakDTOMapper keycloakDTOMapper;
    private final String keycloakUrl;

    public M2MClientDAOImpl(SSOConfigurationProvider ssoConfigurationProvider) {
        keycloakUrl = ssoConfigurationProvider.getSSOUrl();
        keycloakDTOMapper = new M2MKeycloakDTOMapper();
        keycloakHttpClient = new KeycloakHttpClient(keycloakUrl);
    }

    @Override
    public String getClientScopeId(String clientScopeName) {
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES;
        return keycloakHttpClient.getIdBySingleQueryParam("name", clientScopeName, clientScopeUrl);
    }

    @Override
    public String getRealmRoleId(String roleName) {
        String roleUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/roles";
        return keycloakHttpClient.getIdBySingleQueryParam("name", roleName, roleUrl);
    }

    @Override
    public void addClientScopeToClient(String idOfClient, String clientScopeId, boolean optional) {
        String kindOfClientScope = optional ? "optional-client-scopes" : "default-client-scopes";
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient + "/" +kindOfClientScope+ "/" + clientScopeId;

        HttpPut httpPut = new HttpPut();
        try (CloseableHttpClient httpClient = keycloakHttpClient.buildCloseableHttpClient()) {

            URI uri = new URIBuilder(clientScopeUrl).build();
            httpPut.setURI(uri);
            httpPut.addHeader(HttpHeaders.AUTHORIZATION, BEARER + keycloakHttpClient.getAccessToken());

            try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
                keycloakHttpClient.assertNoErrorStatus(response);

                LOG.trace("Added optional client scope successfully");
            }
        } catch (IOException | URISyntaxException | KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to add client scope to client " + idOfClient, e);
        }
    }


    @Override
    public String getIdOfClient(String clientId) {
        String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/clients";
        return keycloakHttpClient.getIdBySingleQueryParam("clientId", clientId, clientUrl);
    }

    @Override
    public void createClient(String clientId) {
        try {
            String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/clients";
            keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(clientUrl), keycloakDTOMapper.createClientRepresentationJson(clientId));
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to create client " + clientId, e);
        }
    }

    @Override
    public void updateClient(String idOfClient, String clientId) {
        try {
            String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient;
            keycloakHttpClient.executeRequestWithJsonBody(new HttpPut(clientUrl), keycloakDTOMapper.createClientRepresentationJson(clientId));
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to update client " + clientId, e);
        }
    }

    @Override
    public void grantRoleToClient(String idOfClient, String roleId, String roleName) {
        String serviceAccountUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient + "/service-account-user";
        String serviceAccountId = keycloakHttpClient.getIdBySingleQueryParam("", "", serviceAccountUrl);
        String grantRoleUrl = keycloakUrl + "/admin/realms/gazelle/users/" + serviceAccountId + "/role-mappings/realm";

        try {
            keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(grantRoleUrl),
                "[" + keycloakDTOMapper.createRealmRoleRepresentationJson(roleId, roleName) + "]");
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to grant role " +roleName+ " to client " + idOfClient, e);
        }
    }
}
