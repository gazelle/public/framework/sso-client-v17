package net.ihe.gazelle.m2m.client.interlay.factory;

import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.KeycloakEnvironmentConfigurationProvider;
import net.ihe.gazelle.m2m.client.interlay.service.AccessTokenCacheService;
import net.ihe.gazelle.m2m.client.interlay.service.apache.HttpKeycloakAccessTokenService;
import net.ihe.gazelle.m2m.client.interlay.service.apache.HttpM2MClientRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.dao.M2MClientDAOImpl;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;

/**
 * This factory will create Services required for M2M.
 * It is meant to be used in context were only environment variables are accessible.
 */
public class M2MServiceFactory {
    SSOConfigurationProvider ssoConfigurationProvider;

    /**
     * Create a factory for M2M services creation.
     *
     * @param k8sidVariableName a String of the name of the environment variable that contains the k8s id.
     */
    public M2MServiceFactory(String k8sidVariableName) {
        ssoConfigurationProvider = new KeycloakEnvironmentConfigurationProvider(k8sidVariableName);
    }


    public ClientM2MRegistrationService getClientM2MClientRegistrationService() {
        return new HttpM2MClientRegistrationService(new M2MClientDAOImpl(ssoConfigurationProvider));
    }

    public AccessTokenService getAccessTokenService() {
        AccessTokenService accessTokenService = new HttpKeycloakAccessTokenService(getClientM2MClientRegistrationService(),
                ssoConfigurationProvider);
        // Wrap the service in a cache
        return new AccessTokenCacheService(accessTokenService);
    }
}
