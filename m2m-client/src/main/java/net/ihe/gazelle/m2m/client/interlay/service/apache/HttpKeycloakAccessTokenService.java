/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenServiceException;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.dao.M2MClientDAOImpl;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static net.ihe.gazelle.m2m.client.interlay.service.apache.HttpM2MClientRegistrationService.ADMIN_ROLE;
import static net.ihe.gazelle.m2m.client.interlay.service.apache.HttpM2MClientRegistrationService.MACHINE_ROLE;

/**
 * This class provides an implementation of AccessTokenService that rely only on Http request for compatibility purposes.
 * @see HttpKeycloakAccessTokenService for an implementation using keycloak-admin-client library.
 */
public class HttpKeycloakAccessTokenService implements AccessTokenService {

    public static final String REALM = "gazelle";
    public static final String REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/" + REALM + "/protocol/openid-connect/token";
    private static final Logger LOG = LoggerFactory.getLogger(HttpKeycloakAccessTokenService.class);
    public static final String CLIENT_CREDENTIALS = "client_credentials";
    public static final String INVALID_SCOPE_ERROR = "invalid_scope";
    public static final String INVALID_CLIENT_ERROR = "invalid_client";
    private final SSOConfigurationProvider ssoConfigurationProvider;

    private final M2MClientDAO m2MClientDAO;
    private final KeycloakHttpClient keycloakHttpClient;
    private final ClientM2MRegistrationService clientM2MClientRegistrationService;

    public HttpKeycloakAccessTokenService(ClientM2MRegistrationService clientM2MClientRegistrationService, SSOConfigurationProvider ssoConfigurationProvider) {
        this.ssoConfigurationProvider = ssoConfigurationProvider;
        m2MClientDAO = new M2MClientDAOImpl(ssoConfigurationProvider);
        keycloakHttpClient = new KeycloakHttpClient(ssoConfigurationProvider.getSSOUrl());
        this.clientM2MClientRegistrationService = clientM2MClientRegistrationService;
    }

    @Override
    public String getAccessToken(String clientScopes) {
        if (clientScopes == null)
            throw new IllegalArgumentException("clientScopes is null");
        return getAccessToken(ssoConfigurationProvider.getClientId(),
                ssoConfigurationProvider.getClientSecret(), clientScopes, true);
    }

    @Override
    public String getAccessToken() {
        return getAccessToken(ssoConfigurationProvider.getClientId(),
                ssoConfigurationProvider.getClientSecret(), null, true);
    }

    private String getAccessToken(String clientId, String clientSecret, String scopes, boolean retryIfFailed) {
        LOG.trace("getAccessToken");
        if (clientId == null || clientSecret == null)
            throw new IllegalArgumentException((clientId == null ? "clientId" : "clientSecret") + " is null");


        String keycloakUrl = ssoConfigurationProvider.getSSOUrl();
        String accessToken;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost accessTokenRequest = new HttpPost(keycloakUrl + REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN);
            List<BasicNameValuePair> params = new ArrayList<>(Arrays.asList(
                    new BasicNameValuePair("client_id", clientId),
                    new BasicNameValuePair("client_secret", clientSecret),
                    new BasicNameValuePair("grant_type", CLIENT_CREDENTIALS)
            ));
            if (scopes != null)
                params.add(new BasicNameValuePair("scope", scopes));
            accessTokenRequest.setEntity(new UrlEncodedFormEntity(params));

            try (CloseableHttpResponse response = httpClient.execute(accessTokenRequest)) {
                //if it failed because of bad scope or unregistered client and retryIfFailed is true, then retry after adding scopes
                if (shouldRetryRequest(retryIfFailed, response)) {
                    String idOfClient = m2MClientDAO.getIdOfClient(clientId);
                    Set<String> roles = Set.of(MACHINE_ROLE, ADMIN_ROLE);
                    if (idOfClient == null)
                        clientM2MClientRegistrationService.registerClient(clientId, clientSecret, roles);

                    return getAccessToken(clientId, clientSecret, scopes, false);
                }

                keycloakHttpClient.assertNoErrorStatus(response);
                HttpEntity responseEntity = response.getEntity();
                accessToken = getAccessTokenFromJson(responseEntity.getContent());
                EntityUtils.consume(responseEntity);
            }
        } catch (IOException e) {
            throw new AccessTokenServiceException("Failed to get access token", e);
        }
        LOG.trace("getAccessToken successful");
        return accessToken;
    }

    private boolean shouldRetryRequest(boolean retryIfFailed, CloseableHttpResponse response) throws IOException {
        return retryIfFailed
                && response.getStatusLine().getStatusCode() >= 400
                && isInvalidScopeOrClientError(response.getEntity().getContent());
    }

    private boolean isInvalidScopeOrClientError(InputStream response) throws IOException {
        String extractedError = extractErrorFromJson(response);
        return extractedError != null && (extractedError.equals(INVALID_SCOPE_ERROR) || extractedError.equals(INVALID_CLIENT_ERROR));
    }

    private String extractErrorFromJson(InputStream response) throws IOException {
        try (response) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("error").asText();
        }
    }

    private String getAccessTokenFromJson(InputStream response) throws IOException {
        try (response) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("access_token").asText();
        }
    }
}