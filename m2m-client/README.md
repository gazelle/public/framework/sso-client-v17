# M2M Client V17

The M2M client library is used by the applications which consume the protected resources.

## How it works

An M2M client in Gazelle is a microservice that needs to consume protected resources 
from another microservice which provides these resources (see [m2m-server](../m2m-server/README.md)).

At startup, the microservice will register itself to the SSO server (Keycloak) as Keycloak client 
(ex : m2m-gazelle-user-management).

When the service try to get protected resources, it will ask for client scopes to the SSO server 
(ex : any-user:read).

With these client scopes, the service will be able to ask for Json Web Tokens (JWT) with the client credentials.

After, the service will be able to ask protected resources by adding the JWT in the Authorization header of the HTTP request.
(ex : Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6IC...)

## [GUIDE] Usage of client library

### Microprofile (resteasy-reactive) Rest Client

The library provides a filter that will handle the RestClient calls. The filter will retrieve the annotations from the method invocation.

If the `@M2MAuthMethod` annotation is present, the filter will add the Authorization header to the request. Otherwise, the request will be sent without the Authorization header.

In consequence, The clients using M2M authentication must be implemented using the RestClient provided by microprofile.
And the methods that need to be secured must be annotated with @M2MAuthMethod.

To activate the M2M registration, set the following property `gzl.m2m.registration.startup.enabled=true` in your application.properties. 
By default, it's disable to avoid bad deployment for applications that does not support `resteasy-reactive`.

1. Annotation the methods that need authentication

```java
@RegisterRestClient(configKey="app-api")
public interface RestClientSecuredImpl {

    @GET
    @Path("/secured")
    @M2MAuthMethod()
    String getOnSecuredEndpoint() {
        [...]
    }

    @GET
    @Path("/unsecured")
    String getOnUnsecuredEndpoint() {
        [...]
    }
}
```

### Apache Http Client

1. There is 3 ways to add the access token to your request as seen below (with example):
- Use the M2MHttpClientBuilder as a classical Apache HttpClientBuilder

```java
List<String> scopes = Collections.singletonList("my-scope");
String k8sIdVariableName = "GZL_XXXX_K8S_ID";

try (CloseableHttpClient client = M2MHttpClientBuilder.createWithAccessToken(scopes,k8sIdVariableName).build()) {

} catch (IOException e) {
    throw new RuntimeException(e);
}
```

- Add the AccessTokenRequestInterceptor to your Apache HttpClient

```java
List<String> scopes = Collections.singletonList("my-scope");
String k8sIdVariableName = "GZL_XXXX_K8S_ID";

try (CloseableHttpClient client = HttpClientBuilder.create()
        .addInterceptorFirst(new AccessTokenRequestInterceptor(scopes,k8sIdVariableName))
        .build()){

} catch (IOException e) {
    throw new RuntimeException(e);
}
```

- Use the AccessTokenProvider to add the token to your request :

```java
SSOConfigurationService ssoConfigurationService = new KeycloakConfigurationServiceImpl();
HttpKeycloakAccessTokenService accessTokenProvider = new HttpKeycloakAccessTokenService(ssoConfigurationService);

List<String> scopes = Collections.singletonList("my-scope");
String accessToken = accessTokenProvider.getAccessToken("myClientID", "myClientSecret", scopes);
```
###  Set required environment variables

| Variable name          | Description                                                   | Example                               |
|------------------------|---------------------------------------------------------------|---------------------------------------|
| GZL_SSO_URL            | The URL of the SSO server.                                    | http://localhost:28080                |
| GZL_SSO_REALM          | The Keycloak realm used by the SSO.                           | gazelle                               |
| GZL_SSO_ADMIN_USER     | The username/email of the used Keycloak admin.                | admin                                 |
| GZL_SSO_ADMIN_PASSWORD | The password of the used Keycloak admin.                      | password                              |
| GZL_M2M_CLIENT_SECRET  | The Keycloak secret of the M2M client.                        | secret                                |
| GZL_XXXX_K8S_ID        | The unique identifier for the instance (used for client name) | gazelle-user-management-12345-1234567 |
| GZL_SERVICE_NAME       | The name of the service (used for apache client)              | gazelle-user-management               |