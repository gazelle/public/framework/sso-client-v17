/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.security.application.acl;

import net.ihe.gazelle.security.application.Permission;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultACLPermissionStoreTest {

    @Test
    void testGetPermissionName() {
        DefaultACLPermissionStore defaultACLPermissionStore = new DefaultACLPermissionStore("permissionName");
        String permissionName = defaultACLPermissionStore.getPermissionName(DefaultACLPermissionStore.READ_ACTION);
        assertEquals("permissionName:read", permissionName);
    }

    @Test
    void testGetPermissions() {
        DefaultACLPermissionStore defaultACLPermissionStore = new DefaultACLPermissionStore("permissionName");
        Set<Permission> permissions = defaultACLPermissionStore.getPermissions();
        Set<String> permissionNames = permissions.stream().map(Permission::getName).collect(Collectors.toSet());
        assertTrue(permissionNames.contains("permissionName:create"));
    }
}
