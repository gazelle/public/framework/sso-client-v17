/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.domain.metadata;

import net.ihe.gazelle.sso.api.domain.metadata.AuthzScope;
import net.ihe.gazelle.sso.api.domain.metadata.SecuredInterface;
import net.ihe.gazelle.sso.api.domain.metadata.SecuredMethod;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SecuredInterfaceTest {

    @Test
    void testConstructor() {
        SecuredInterface securedInterface = new SecuredInterface();
        assertTrue(securedInterface.getAuthzScopes().isEmpty());
        assertTrue(securedInterface.getSecuredMethods().isEmpty());
        assertFalse(securedInterface.isSecured());
    }

    @Test
    void testSettersAndGetters() {
        SecuredInterface securedInterface = new SecuredInterface();
        securedInterface.setSecured(true);
        securedInterface.setAuthzScopes(List.of(new AuthzScope("scope1","description1")));
        securedInterface.setSecuredMethods(List.of(SecuredMethod.M2M));

        assertEquals("scope1",securedInterface.getAuthzScopes().get(0).getName());
        assertTrue(securedInterface.isSecured());
        assertTrue(securedInterface.getSecuredMethods().contains(SecuredMethod.M2M));

        securedInterface.setAuthzScopes(null);
        assertTrue(securedInterface.getAuthzScopes().isEmpty());
        securedInterface.setSecuredMethods(null);
        assertTrue(securedInterface.getSecuredMethods().isEmpty());
    }

    @Test
    void testSecuredMethod() {
        assertEquals("m2m", SecuredMethod.M2M.getName());
    }
}
