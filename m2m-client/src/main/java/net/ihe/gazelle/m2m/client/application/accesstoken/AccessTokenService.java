package net.ihe.gazelle.m2m.client.application.accesstoken;


public interface AccessTokenService {

    /**
     * Get access token using the client credentials flow
     *
     * @param clientScopes the client scopes (separated by a whitespace) of the access token
     * @return the String of a Json Web Token
     * @throws IllegalArgumentException    if clientScopes is null
     * @throws AccessTokenServiceException when unable to retrieve the access token
     */
    String getAccessToken(String clientScopes);

    /**
     * Get access token using the client credentials flow
     *
     * @return the String of a Json Web Token
     * @throws IllegalArgumentException    if clientScopes is null
     * @throws AccessTokenServiceException when unable to retrieve the access token
     */
    String getAccessToken();
}

