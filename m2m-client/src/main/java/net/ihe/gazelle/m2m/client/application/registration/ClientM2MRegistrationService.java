package net.ihe.gazelle.m2m.client.application.registration;

import java.util.Set;

/**
 * Component used to register an M2M client to an SSO server
 */
public interface ClientM2MRegistrationService {


    /**
     * Perform the registration of the client with the given role
     *
     * @param clientId     the id of the client
     * @param clientSecret the secret of the client
     * @param roles        the roles to assign to the new client
     */
    void registerClient(String clientId, String clientSecret, Set<String> roles);
}
