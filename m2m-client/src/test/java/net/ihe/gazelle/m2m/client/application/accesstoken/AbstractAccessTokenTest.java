package net.ihe.gazelle.m2m.client.application.accesstoken;

import com.github.tomakehurst.wiremock.WireMockServer;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This abstract class is used to test multiple implementation of AccessTokenService while using the same tests.
 *
 * @see AccessTokenService
 */
public abstract class AbstractAccessTokenTest {

    public static final String KEYCLOAK_URL = "http://localhost:12345";
    public static final String KEYCLOAK_REALM = "gazelle";
    public static final String ADMIN_USER = "admin";
    public static final String ADMIN_PASSWORD = "test";
    private static WireMockServer wireMockServer;
    private static AccessTokenService accessTokenService;
    protected static SSOConfigurationProvider ssoConfigurationProvider = new SSOConfigurationProviderMock();

    /**
     * This method will return the implementation of the AccessTokenService that will be tested
     *
     * @return an AccessTokenService
     */
    protected abstract AccessTokenService getAccessTokenService();

    @BeforeAll
    public static void setupAll() {
        wireMockServer = new WireMockServer(12345);
        wireMockServer.start();
    }

    @BeforeEach
    public void setup() {
        accessTokenService = getAccessTokenService();
    }

    @Test
    void testGetAccessTokenWithBadParameters() {
        assertThrows(IllegalArgumentException.class, () -> accessTokenService.getAccessToken(null));
    }

    @Test
    void testGetAccessTokenWithScope() {
        String url = "/realms/gazelle/protocol/openid-connect/token";

        wireMockServer.stubFor(post(url)
                .withRequestBody(containing("client_id=" + ssoConfigurationProvider.getClientId()))
                .withRequestBody(containing("client_secret=clientSecret"))
                .withRequestBody(containing("scope=scope"))
                .willReturn(ok("{\"access_token\":\"token\"}")));


        String accessToken = accessTokenService.getAccessToken("scope");
        assertEquals("token", accessToken);
    }

    @Test
    void testGetAccessTokenNoScope() {
        String url = "/realms/gazelle/protocol/openid-connect/token";

        wireMockServer.stubFor(post(url)
                .withRequestBody(containing("client_id=" + ssoConfigurationProvider.getClientId()))
                .withRequestBody(containing("client_secret=clientSecret"))
                .withRequestBody(notContaining("scope=scope"))
                .willReturn(ok("{\"access_token\":\"token\"}")));

        String accessToken = accessTokenService.getAccessToken();
        assertEquals("token", accessToken);
    }

    @AfterAll
    static void tearDown() {
        wireMockServer.stop();
    }
}
