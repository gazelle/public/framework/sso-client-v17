package net.ihe.gazelle.m2m.client.interlay.filter;

import org.apache.http.impl.client.HttpClientBuilder;

import java.util.List;

public class M2MHttpClientBuilder extends HttpClientBuilder {

    /**
     * Create a builder that will automatically add an access token to the request
     * @return return an HttpClientBuilder with an HttpRequestInterceptor that add access token
     */
    public static HttpClientBuilder createWithAccessToken(String k8sIdVariableName) {
        return HttpClientBuilder.create().addInterceptorFirst(
                new AccessTokenRequestInterceptor(k8sIdVariableName));
    }

}
