/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.dto.metadata;

import net.ihe.gazelle.sso.api.domain.metadata.SecuredInterface;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;

public class SecuredDTOFactory implements DTOFactory<SecuredInterfaceDTO, SecuredInterface>{

    private final DTOFactoryProvider<BindingDTO> factoryProvider = new DTOFactoryProviderSPI<>();

    @Override
    public SecuredInterfaceDTO createDTO(SecuredInterface object) {
        return new SecuredInterfaceDTO()
                .setInterfaceName(object.getInterfaceName())
                .setRequired(object.isRequired())
                .setInterfaceVersion(object.getInterfaceVersion())
                .setBindings(
                        object.getBindings() != null ?
                                object.getBindings()
                                        .stream()
                                        .map(binding ->
                                            ((DTOFactory<BindingDTO, Binding>) factoryProvider
                                                    .createFactory(binding.getClass()))
                                                    .createDTO(binding))
                                        .toList() : null
                )
                .setAuthzScopes(object.getAuthzScopes())
                .setSecuredMethods(object.getSecuredMethods())
                .setSecured(object.isSecured())
                ;
    }

    @Override
    public Class<SecuredInterface> getType() {
        return SecuredInterface.class;
    }
}
