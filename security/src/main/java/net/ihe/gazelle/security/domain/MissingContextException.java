package net.ihe.gazelle.security.domain;

import java.io.Serial;

public class MissingContextException extends RuntimeException {

   @Serial
   private static final long serialVersionUID = 6335455290204416732L;

   public MissingContextException() {
   }

   public MissingContextException(String message) {
      super(message);
   }

   public MissingContextException(String message, Throwable cause) {
      super(message, cause);
   }

   public MissingContextException(Throwable cause) {
      super(cause);
   }
}
