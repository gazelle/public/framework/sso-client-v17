package net.ihe.gazelle.m2m.client.interlay;

import jakarta.enterprise.context.RequestScoped;
import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;
import net.ihe.gazelle.sso.api.interlay.converter.CleanedString;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@RequestScoped
public class KeycloakPropertyConfigurationProvider implements SSOConfigurationProvider {

    @ConfigProperty(name = "gzl.sso.url")
    CleanedString ssoUrl;

    @ConfigProperty(name = "gzl.sso.realm")
    CleanedString ssoRealm;

    @ConfigProperty(name = "gzl.sso.admin.user")
    CleanedString ssoAdminUser;

    @ConfigProperty(name = "gzl.sso.admin.password")
    CleanedString ssoAdminPassword;

    @ConfigProperty(name = "gzl.k8s.id")
    CleanedString k8sId;
    @ConfigProperty(name = "gzl.service.name")
    CleanedString serviceName;
    @ConfigProperty(name = "gzl.m2m.client.secret")
    CleanedString clientSecret;

    @Override
    public String getSSOUrl() {
        return ssoUrl.getString();
    }

    @Override
    public String getSSORealm() {
        return ssoRealm.getString();
    }

    @Override
    public String getSSOAdminUser() {
        return ssoAdminUser.getString();
    }

    @Override
    public String getSSOAdminPassword() {
        return ssoAdminPassword.getString();
    }

    @Override
    public String getK8sId() {
        return k8sId.getString();
    }

    @Override
    public String getServiceName() {
        return serviceName.getString();
    }

    @Override
    public String getClientSecret() {
        return clientSecret.getString();
    }

}