# sso-client

This library is used by Gazelle web applications to authenticate users and machines with an SSO server or by IP
Login. For JDK-21 stack.

For JDK-17 please use the sso-client-v17 library (deprecated)

>:warning: **Warning:** This library currently depends on Quarkus BOM in version 3.8.4.Final.

<!-- TOC -->
* [sso-client](#sso-client)
  * [Introduction](#introduction)
  * [Build and tests](#build-and-tests)
  * [Add Maven dependency](#add-maven-dependency)
  * [Modules](#modules)
    * [SSO Client API](#sso-client-api)
    * [M2M Client](#m2m-client)
    * [OIDC Server](#oidc-server)
    * [Security](#security-)
  * [License](#license)
<!-- TOC -->

## Introduction

The project contains 4 modules:

- **sso-client-api** - The API for SSO client and server applications.
- **sso-server**     - The server library for common implementations regarding SSO authentication.
- **m2m-client**     - The client library for machine to machine authentication.
- **oidc-server**    - The server library for OIDC authentication (M2M + user auths).

The equivalent of this library for **JDK 7** is [here](https://gitlab.inria.fr/gazelle/library/sso-client-v7).

## Build and tests

The project must be build using JDK-21.

```bash
mvn clean install
```

You can find a **Jacoco** report in the `target/site` folder of each module.

A Sonar report is available [here](https://gazelle.ihe.net/sonarqube/dashboard?id=net.ihe.gazelle%253Asso-client).

## Add Maven dependency

Add the sso-client in your parent pom.xml as a managed dependency.

```xml
<dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>net.ihe.gazelle</groupId>
      <artifactId>sso-client</artifactId>
      <version>${sso.client.version}</version>
      <type>pom</type>
      <scope>import</scope>
    </dependency>
  </dependencies>
</dependencyManagement>
```

Then, you can add the dependency of the module(s) you need without specifying the version.

## Modules

### SSO Client API

This library provides APIs useful for applications using sso-client.

Import APIs in your project:

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>sso-client-api</artifactId>
</dependency>
```

### M2M Client

This library is used by the applications which consume the protected resources via M2M.

Example : The Gazelle Test Management application consumes user resources.

```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>m2m-client</artifactId>
</dependency>
```

Link to the documentation of the client library: [m2m-client](m2m-client/README.md)

### OIDC Server

This library is used by the applications which provide the protected resources via OIDC.

Example : The Gazelle-user-management application provides user resources by REST API.

```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>oidc-server</artifactId>
</dependency>
```

Link to the documentation of the client library: [oidc-server](oidc-server/README.md)


### Security 

This library is used to provide authentication and authorization.

```xml
<dependency>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>security</artifactId>
</dependency>
```

Link to the documentation of the client library: [security](security/README.md)


## License

```
    Copyright 2022-2025 IHE International
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
