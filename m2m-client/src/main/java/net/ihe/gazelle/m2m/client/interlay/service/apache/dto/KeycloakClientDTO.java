/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class KeycloakClientDTO {

    private String clientId;
    private final String protocol;
    private String baseUrl;
    private String clientAuthenticatorType;
    private List<String> protocolMappers = new ArrayList<>();
    private List<String> redirectUris = new ArrayList<>();
    private String name;
    private List<String> defaultClientScopes = new ArrayList<>();
    private List<String> optionalClientScopes = new ArrayList<>();

    public KeycloakClientDTO(String protocol) {
        this.protocol = protocol;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getClientAuthenticatorType() {
        return clientAuthenticatorType;
    }

    public void setClientAuthenticatorType(String clientAuthenticatorType) {
        this.clientAuthenticatorType = clientAuthenticatorType;
    }

    public List<String> getProtocolMappers() {
        return new ArrayList<>(protocolMappers);
    }

    public void setProtocolMappers(List<String> protocolMappers) {
        this.protocolMappers = new ArrayList<>(protocolMappers);
    }

    public void setRedirectUris(List<String> redirectUris) {
        this.redirectUris = new ArrayList<>(redirectUris);
    }

    public List<String> getRedirectUris() {
        return new ArrayList<>(redirectUris);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<String> getDefaultClientScopes() {
        return new ArrayList<>(defaultClientScopes);
    }

    public void setDefaultClientScopes(List<String> defaultClientScopes) {
        this.defaultClientScopes = new ArrayList<>(defaultClientScopes);
    }

    public List<String> getOptionalClientScopes() {
        return new ArrayList<>(optionalClientScopes);
    }

    public void setOptionalClientScopes(List<String> optionalClientScopes) {
        this.optionalClientScopes = new ArrayList<>(optionalClientScopes);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeycloakClientDTO that = (KeycloakClientDTO) o;
        return Objects.equals(clientId, that.clientId)
                && Objects.equals(protocol, that.protocol)
                && Objects.equals(baseUrl, that.baseUrl)
                && Objects.equals(clientAuthenticatorType, that.clientAuthenticatorType)
                && Objects.equals(protocolMappers, that.protocolMappers)
                && Objects.equals(redirectUris, that.redirectUris)
                && Objects.equals(name, that.name)
                && Objects.equals(defaultClientScopes, that.defaultClientScopes)
                && Objects.equals(optionalClientScopes, that.optionalClientScopes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                clientId,
                protocol,
                baseUrl,
                clientAuthenticatorType,
                protocolMappers,
                redirectUris,
                name,
                defaultClientScopes,
                optionalClientScopes
        );
    }
}
