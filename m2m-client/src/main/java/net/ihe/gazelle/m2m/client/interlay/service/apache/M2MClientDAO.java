/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache;


public interface M2MClientDAO {

    /**
     * Get the id of a client scope by its name
     * @param name the name of the client scope
     * @return the id of the client scope
     */
    String getClientScopeId(String name);

    /**
     * Get the id of a realm role by its name
     * @param roleName the name of the realm role
     * @return the id of the realm role
     */
    String getRealmRoleId(String roleName);

    /**
     * Add a client scope to a client
     * @param idOfClient the id of the client
     * @param clientScopeId the id of the client scope
     * @param optional the client scope is added by default or optional
     * @throws M2MClientDAOException if the operation fails
     */
    void addClientScopeToClient(String idOfClient, String clientScopeId, boolean optional);

    /**
     * Get the id of a client by its id
     * @param clientId the name of the client
     * @return the id of the client
     */
    String getIdOfClient(String clientId);

    /**
     * Create a client in Keycloak
     * @param clientId the clientId of the created client
     * @throws M2MClientDAOException if the operation fails
     */
    void createClient(String clientId);

    /**
     * Update an existing client in Keycloak
     * @param idOfClient the technical id of the client
     * @param clientId the clientId of the client to update
     * @throws M2MClientDAOException if the operation fails
     */
    void updateClient(String idOfClient, String clientId);

    /**
     * Grant a role to an existing client
     * @param idOfClient the technical id the client
     * @param roleId the id of the role to grant
     * @param roleName the name of the role to grant
     * @throws M2MClientDAOException if the operation fails
     */
    void grantRoleToClient(String idOfClient, String roleId, String roleName);
}
