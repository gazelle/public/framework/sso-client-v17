/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.mock;

import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;

public class SSOConfigurationProviderMock implements SSOConfigurationProvider {
    private String k8sId;
    private String serviceName;

    public SSOConfigurationProviderMock() {
    }

    public SSOConfigurationProviderMock(String k8sId, String serviceName) {
        this.k8sId = k8sId;
        this.serviceName = serviceName;
    }
    @Override
    public String getSSOUrl() {
        //No implementation needed
        return null;
    }

    @Override
    public String getSSORealm() {
        //No implementation needed
        return null;
    }

    @Override
    public String getSSOAdminUser() {
        //No implementation needed
        return null;
    }

    @Override
    public String getSSOAdminPassword() {
        //No implementation needed
        return null;
    }

    @Override
    public String getK8sId() {
        return k8sId;
    }

    @Override
    public String getServiceName() {
        return serviceName;
    }

    @Override
    public String getClientSecret() {
        //No implementation needed
        return null;
    }
}
