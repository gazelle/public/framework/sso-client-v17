package net.ihe.gazelle.security.domain;

import java.io.Serial;

public class UnauthorizedException extends RuntimeException {
   @Serial
   private static final long serialVersionUID = 9173471529397073279L;

   public UnauthorizedException() {
      super();
   }
}
