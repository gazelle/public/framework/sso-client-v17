package net.ihe.gazelle.security.interlay;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import net.ihe.gazelle.security.application.AuthzImpl;
import net.ihe.gazelle.security.domain.Authz;

@ApplicationScoped
public class AuthzCDIFactory {

   @Produces
   @ApplicationScoped
   public Authz getAuthz() {
      return new AuthzImpl(new PermissionStoreSPIProvider());
   }

}
