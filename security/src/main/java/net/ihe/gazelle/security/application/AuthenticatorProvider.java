package net.ihe.gazelle.security.application;

import java.util.List;

public interface AuthenticatorProvider {

   /**
    * Get the authenticators to be used to authenticate the entity accessing the application.
    * @return the authenticators to be used to authenticate the entity accessing the application
    */
   List<Authenticator> getAuthenticators();

}
