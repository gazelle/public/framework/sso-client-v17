package net.ihe.gazelle.security.interlay;

import net.ihe.gazelle.security.application.PermissionStore;
import net.ihe.gazelle.security.application.PermissionStoreProvider;

import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

public class PermissionStoreSPIProvider implements PermissionStoreProvider {
   @Override
   public Set<PermissionStore> getPermissionStores() {
      ServiceLoader<PermissionStore> serviceLoader = ServiceLoader.load(PermissionStore.class);
      return serviceLoader.stream()
            .map(ServiceLoader.Provider::get)
            .collect(Collectors.toSet());
   }

}
