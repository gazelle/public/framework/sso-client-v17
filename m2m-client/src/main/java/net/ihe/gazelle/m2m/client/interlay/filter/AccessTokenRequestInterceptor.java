package net.ihe.gazelle.m2m.client.interlay.filter;

import net.ihe.gazelle.m2m.client.application.accesstoken.AccessTokenService;
import net.ihe.gazelle.m2m.client.interlay.factory.M2MServiceFactory;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;

import java.util.ArrayList;
import java.util.List;

/**
 * This interceptor is used to add a JWT access token to an HttpRequest before sending it.
 */
public class AccessTokenRequestInterceptor implements HttpRequestInterceptor {

    AccessTokenService accessTokenService;

    public AccessTokenRequestInterceptor(String k8sIdVariableName) {
        this.accessTokenService = new M2MServiceFactory(k8sIdVariableName).getAccessTokenService();
    }

    @Override
    public void process(HttpRequest request, HttpContext context) {
        request.addHeader(
                new BasicHeader(
                        HttpHeaders.AUTHORIZATION, "Bearer " +
                        accessTokenService.getAccessToken()
                )
        );
    }
}
