/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.application;

import java.util.List;
import java.util.Map;
import net.ihe.gazelle.sso.api.domain.metadata.AuthzScope;

public interface SSOScopeProvider {

    /**
     * Get a map of scopes provided by the application
     * @return the map of interfaces containing the list of scopes
     */
    Map<String,List<AuthzScope>> getAuthzScopes();
}
