/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.oidc.server;

import net.ihe.gazelle.oidc.server.interlay.mock.OIDCMockJsonWebToken;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OIDCIdentityTest {

    private static final JsonWebToken jsonWebToken = new OIDCMockJsonWebToken();

    private static OIDCIdentity oidcIdentity;

    @BeforeAll
    static void setUp() {
        oidcIdentity = new OIDCIdentity(jsonWebToken);
    }

    @Test
    void testIsLoggedIn() {
        assertTrue(oidcIdentity.isAuthenticated());
        assertEquals("oidc-test-mocked-jwt",oidcIdentity.getId());
        assertEquals("nom donné nom de famille",oidcIdentity.getName());
        assertTrue(oidcIdentity.getOrganizationId().contains("organizationKeyword"));
        assertEquals("email@mocked.fr",oidcIdentity.getEmail());
    }

    @Test
    void testRole() {
        assertTrue(oidcIdentity.hasGroup("admin_role"));
        assertFalse(oidcIdentity.hasGroup("badRole"));
    }
}