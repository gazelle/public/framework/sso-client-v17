package net.ihe.gazelle.m2m.client.application.registration;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.Scenario;
import net.ihe.gazelle.m2m.client.interlay.service.resteasy.ClientM2MRegistrationServiceImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class ClientM2MRegistrationServiceTest {

    private static final String GAZELLE = "gazelle";
    public static final String ADMIN_REALMS_GAZELLE_ROLES = "/admin/realms/" + GAZELLE + "/roles";
    public static final String ADMIN_REALM_GAZELLE_CLIENTS = "/admin/realms/" + GAZELLE + "/clients";
    public static final String ADMIN_REALM_GAZELLE_USERS = "/admin/realms/" + GAZELLE + "/users";
    private static ClientM2MRegistrationService m2MClientRegistrationService;

    private static WireMockServer wireMockServer;

    private final static String accessToken = "accesToken";


    @BeforeAll
    public static void setUp() {
        wireMockServer = new WireMockServer(12345);
        wireMockServer.start();

        wireMockReturnOkJson(WireMock.post("/realms/master/protocol/openid-connect/token"), "{\"access_token\":\"" + accessToken + "\"}");

        String keycloakUrl = "http://localhost:12345";
        String keycloakRealm = "gazelle";
        String keycloakAdminUser = "admin";
        String keycloakAdminPassword = "admin";
        m2MClientRegistrationService = new ClientM2MRegistrationServiceImpl(keycloakUrl,keycloakRealm,keycloakAdminUser,keycloakAdminPassword);
    }

    @AfterAll
    public static void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void testCreateClient() {
        String clientId = "clientId";
        String idServiceAccount = "idServiceAccount";
        Set<String> roles = Set.of("roleName");

        wireMockReturnOkJson(WireMock.get(ADMIN_REALM_GAZELLE_CLIENTS + "?clientId=" + clientId)
                .inScenario("CreateClient")
                .whenScenarioStateIs(Scenario.STARTED)
                .willSetStateTo("step1")
                , "[]");

        wireMockReturnOkJson(WireMock.get(ADMIN_REALM_GAZELLE_CLIENTS + "?clientId=" + clientId)
                .inScenario("CreateClient")
                .whenScenarioStateIs("step1"),
                "[{\"id\":\"idTest\",\"clientId\":\"" + clientId + "\"}]");

        wireMockReturnOkJson(WireMock.get(ADMIN_REALM_GAZELLE_CLIENTS + "/idTest/service-account-user"),
                "{\"id\":\"" + idServiceAccount + "\",\"username\":\"service-account\",\"realmRoles\":[\"defaultRoleName\"]}");


        wireMockServer.stubFor(WireMock.post(ADMIN_REALM_GAZELLE_CLIENTS).willReturn(WireMock.created()));
        wireMockReturnOkJson(WireMock.get(ADMIN_REALMS_GAZELLE_ROLES + "/" + roles),"{\"id\":\"idTest\",\"name\":\"" + roles + "\"}");
        wireMockServer.stubFor(WireMock.post(ADMIN_REALM_GAZELLE_USERS + "/" + idServiceAccount + "/role-mappings/realm").willReturn(WireMock.ok()));

        assertDoesNotThrow(() -> m2MClientRegistrationService.registerClient(clientId, "secret", roles));
    }

    @Test
    void testUpdateClient() {
        String clientId = "clientId";
        String idOfClientId = "idClient";
        String roleName = "roleName";
        Set<String> roles = Set.of(roleName);
        String authzScopeNames = "authzScopeNames";
        wireMockReturnOkJson(WireMock.get(ADMIN_REALM_GAZELLE_CLIENTS + "?clientId=" + clientId),
                "[{\"id\":\"" +idOfClientId+ "\",\"clientId\":\"" + clientId + "\"}]");

        String idServiceAccount = "idServiceAccount";
        wireMockReturnOkJson(WireMock.get(ADMIN_REALM_GAZELLE_CLIENTS + "/" +idOfClientId+"/service-account-user"),
                "{\"id\":\"" + idServiceAccount + "\",\"username\":\"service-account\"}");

        wireMockServer.stubFor(WireMock.put(ADMIN_REALM_GAZELLE_CLIENTS + "/" + idOfClientId).willReturn(WireMock.ok()));
        wireMockServer.stubFor(WireMock.put(ADMIN_REALM_GAZELLE_CLIENTS + "/" + idOfClientId).willReturn(WireMock.ok()));

        String stubClientScopes = "[{\"id\":\"m2m-client-scope-id\", \"name\": \"m2m-client-scope\"},{\"id\": \"microprofile-jwt-id\", \"name\": \"microprofile-jwt\"}]";
        wireMockServer.stubFor(WireMock.get("/admin/realms/" + GAZELLE + "/client-scopes").willReturn(WireMock.ok(stubClientScopes).withHeader("Content-Type","application/json")));
        wireMockServer.stubFor(WireMock.put(ADMIN_REALM_GAZELLE_CLIENTS + "/" + idOfClientId + "/default-client-scopes/m2m-client-scope-id").willReturn(WireMock.ok()));
        wireMockServer.stubFor(WireMock.put(ADMIN_REALM_GAZELLE_CLIENTS + "/" + idOfClientId + "/default-client-scopes/microprofile-jwt-id").willReturn(WireMock.ok()));
        wireMockReturnOkJson(WireMock.get(ADMIN_REALMS_GAZELLE_ROLES + "/" + roleName),"{\"id\":\"idTest\",\"name\":\"" + roleName + "\"}");
        wireMockServer.stubFor(WireMock.post(ADMIN_REALM_GAZELLE_USERS + "/" + idServiceAccount + "/role-mappings/realm").willReturn(WireMock.ok()));

        wireMockReturnOkJson(WireMock.get(ADMIN_REALMS_GAZELLE_ROLES + "/" + roleName),"{\"id\":\"idTest\",\"name\":\"" + roleName + "\"}");
        assertDoesNotThrow(() -> m2MClientRegistrationService.registerClient(clientId, "secret",roles));
    }


    private static void wireMockReturnOkJson(MappingBuilder builder, String json) {
        wireMockServer.stubFor(builder.willReturn(WireMock.ok()
                .withHeader("Content-type", "application/json")
                .withBody(json)
        ));
    }
}
