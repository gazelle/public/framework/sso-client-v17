package net.ihe.gazelle.security.domain;

public interface ProtectedResource {

   AccessControlList getAccessControlList();

}
