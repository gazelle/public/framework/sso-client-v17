/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.security.application;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RBACPoliciesTest {

    @Test
    void testRBACPolicyBadPermission() {
        RBACPolicy rbacPolicy = new RBACPolicy("badPermission");
        assertThrows(IllegalStateException.class, () -> rbacPolicy.evaluate(null));}

    @Test
    void testRBACPolicy() {
        RBACPolicy rbacPolicy = new RBACPolicy("permission");
        BaseGazelleIdentity gazelleIdentity = new BaseGazelleIdentity(new PrincipalTester("tester")).setGroups(Set.of("role:monitor"));
        assertTrue(rbacPolicy.evaluate(gazelleIdentity, "role:monitor"));

        gazelleIdentity.setGroups(Set.of("role:notAllowed"));
        assertFalse(rbacPolicy.evaluate(gazelleIdentity,"role:notAllowed"));
        assertFalse(rbacPolicy.evaluate(gazelleIdentity,"role:test_particpant"));
    }
}
