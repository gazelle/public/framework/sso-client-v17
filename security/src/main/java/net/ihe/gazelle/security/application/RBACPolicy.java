package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.util.Set;
import java.util.stream.Collectors;

public class RBACPolicy implements Policy {

   private final String permissionName;

   public RBACPolicy(String permissionName) {
      this.permissionName = permissionName;
   }
   //FIXME instance dependency to the matrix may be an issue
   RBACMatrix matrix = new RBACMatrix();
   @Override
   public boolean evaluate(GazelleIdentity identity, Object... context) {
      Set<String> allowedGroups = matrix.getAllowedGroups(permissionName);
      allowedGroups = allowedGroups.stream().map(s -> s.replace("{}", (String) context[0])).collect(Collectors.toSet());
      return allowedGroups.stream().anyMatch(allowedGroup -> identity.getGroups().contains(allowedGroup));
   }
}
