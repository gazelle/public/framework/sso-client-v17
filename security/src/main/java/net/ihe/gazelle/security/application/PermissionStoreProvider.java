package net.ihe.gazelle.security.application;

import java.util.Set;

/**
 * PermissionStoreProvider is used by the Authorization framework to retrieve all permission stores declared in this
 * application.
 */
public interface PermissionStoreProvider {
    Set<PermissionStore> getPermissionStores();
}
