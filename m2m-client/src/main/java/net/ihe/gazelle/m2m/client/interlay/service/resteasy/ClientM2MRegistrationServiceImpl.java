/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.resteasy;

import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationException;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ClientScopeRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ClientM2MRegistrationServiceImpl implements ClientM2MRegistrationService {

    Logger log = LoggerFactory.getLogger(ClientM2MRegistrationServiceImpl.class);

    private final Keycloak keycloak;
    private final String keycloakRealm;
    private RealmResource realm;

    public ClientM2MRegistrationServiceImpl(String keycloakUrl, String keycloakRealm, String keycloakAdminUser, String keycloakAdminPassword) {
        this.keycloakRealm = keycloakRealm;

        this.keycloak = KeycloakBuilder.builder()
                .serverUrl(keycloakUrl)
                .grantType(OAuth2Constants.PASSWORD)
                .realm("master")
                .clientId("admin-cli")
                .username(keycloakAdminUser)
                .password(keycloakAdminPassword)
                .build();
    }

    @Override
    public void registerClient(String clientId, String clientSecret, Set<String> roles) {
        log.warn("Registering M2M client in Keycloak : {}", clientId);
        try {
            keycloak.tokenManager().getAccessToken();
            realm = keycloak.realm(keycloakRealm);
        } catch (Exception e) {
            throw new ClientM2MRegistrationException("Unable to connect to Keycloak", e);
        }

        List<ClientRepresentation> clientRepresentations = realm.clients().findByClientId(clientId);

        ClientRepresentation clientRepresentation = generateClientRepresentation(clientId,clientSecret);
        // Create client if not exists
        String idOfClient;
        if (clientRepresentations.isEmpty()) {
            realm.clients().create(clientRepresentation);
            ClientRepresentation newClientRepresentations = realm.clients().findByClientId(clientId).get(0);
            idOfClient = newClientRepresentations.getId();
        // If exists, retrieve client
        } else {
            idOfClient = clientRepresentations.get(0).getId();
            realm.clients().get(idOfClient).update(clientRepresentation);
            List<ClientScopeRepresentation> clientScopeRepresentations = realm.clientScopes().findAll();
            addClientScopeToClient(idOfClient, clientScopeRepresentations,"microprofile-jwt");
            addClientScopeToClient(idOfClient, clientScopeRepresentations,"m2m-client-scope");
        }

        // Add roles to service account user
        roles.forEach(roleName -> grantRoleToServiceAccountUser(idOfClient, roleName));
    }

    private void addClientScopeToClient(String idOfClient, List<ClientScopeRepresentation> clientScopeRepresentations, String clientScopeName) {
        Optional<ClientScopeRepresentation> optionalClientScope = clientScopeRepresentations.stream().filter((clientScope) -> clientScope.getName().equals(clientScopeName)).findFirst();
        optionalClientScope.ifPresent(clientScopeRepresentation -> realm.clients().get(idOfClient).addDefaultClientScope(clientScopeRepresentation.getId()));
    }

    /**
     * Update the role of the service account user representing the client
     *
     * @param clientId the id of the client
     * @param roleName the name of the role
     */
    private void grantRoleToServiceAccountUser(String clientId, String roleName) {
        UserRepresentation userRepresentation = realm.clients().get(clientId).getServiceAccountUser();
        RoleRepresentation roleRepresentation = realm.roles().get(roleName).toRepresentation();
        realm.users().get(userRepresentation.getId()).roles().realmLevel().add(List.of(roleRepresentation));
    }

    /**
     * Generate a ClientRepresentation
     * @param clientId the clientId
     * @param clientSecret the secret of the client
     * @return the ClientRepresentation
     */
    private static ClientRepresentation generateClientRepresentation(String clientId, String clientSecret) {
        ClientRepresentation clientRepresentation = new ClientRepresentation();
        clientRepresentation.setAuthorizationServicesEnabled(false);
        clientRepresentation.setClientAuthenticatorType("client-secret");
        clientRepresentation.setName(clientId);
        clientRepresentation.setDefaultClientScopes(List.of("microprofile-jwt", "m2m-client-scope"));
        clientRepresentation.setClientId(clientId);
        clientRepresentation.setDirectAccessGrantsEnabled(false);
        clientRepresentation.setPublicClient(false);
        clientRepresentation.setSecret(clientSecret);
        clientRepresentation.setServiceAccountsEnabled(true);
        clientRepresentation.setStandardFlowEnabled(false);
        return clientRepresentation;
    }
}
