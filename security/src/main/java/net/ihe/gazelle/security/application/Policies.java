package net.ihe.gazelle.security.application;

public class Policies {

   public static Policy and(Policy policy1, Policy policy2) {
      return (identity, context) -> policy1.evaluate(identity, context) && policy2.evaluate(identity, context);
   }

   public static Policy or(Policy policy1, Policy policy2) {
      return (identity, context) -> policy1.evaluate(identity, context) || policy2.evaluate(identity, context);
   }

   public static Policy isLoggedIn() {
      return (identity, context) -> identity.isAuthenticated();
   }

   public static Policy isAdmin() {
      return and(isLoggedIn(), hasGroupAdmin());
   }

   private static Policy hasGroupAdmin() {
      return ((identity, context) -> identity.hasGroup("role:gazelle_admin"));
   }

   private Policies() {
      // hide the constructor, all mathods are static.
   }

}
