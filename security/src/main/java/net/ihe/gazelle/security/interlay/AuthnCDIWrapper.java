package net.ihe.gazelle.security.interlay;


import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import net.ihe.gazelle.security.application.AuthnImpl;
import net.ihe.gazelle.security.domain.Authn;
import net.ihe.gazelle.security.domain.GazelleIdentity;

@ApplicationScoped
public class AuthnCDIWrapper implements Authn {

   private final Authn authn;

   public AuthnCDIWrapper() {
      this.authn = new AuthnImpl(new AuthenticatorSPIProvider());
   }

   @Produces
   @Override
   @RequestScoped
   public GazelleIdentity getIdentity() {
      return authn.getIdentity();
   }

}
