package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.util.Objects;

/**
 * A permission is the association of a named controlled access or named action with a Policy to take the decision
 * whether the permission can be granted or not.
 * Name of the action
 * Policy to decide if the permission to perform the action can be granted or not.
 */
public class Permission {

   private final String name;
   private final Policy policy;

   public Permission(String name, Policy policy) {
      this.name = name;
      this.policy = policy;
   }

   public String getName() {
      return name;
   }
   public boolean isGranted(GazelleIdentity identity, Object... context) {
      return policy.evaluate(identity, context);
   }

   @Override
   public boolean equals(Object o) {
      //Equals is only based on permission name to ensure unicity per name. This equals will be called by the
      // aggregated Set
      // of all permissions in AuthzImpl.
      if (this == o) {
         return true;
      }
      if (!(o instanceof Permission that)) {
         return false;
      }
      return Objects.equals(name, that.name);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name);
   }
}
