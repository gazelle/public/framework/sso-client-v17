/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.dto.metadata;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.sso.api.domain.metadata.AuthzScope;
import net.ihe.gazelle.sso.api.domain.metadata.SecuredMethod;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.InterfaceDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.factory.json.AbstractInterfaceJsonFactory;

import java.util.ArrayList;
import java.util.List;

public class SecuredInterfaceJsonFactory extends AbstractInterfaceJsonFactory {

    @Override
    public InterfaceDTO createStructure(ObjectNode objectNode) {

        return new SecuredInterfaceDTO()
                .setBindings(getBindings(objectNode))
                .setInterfaceName(getStringField(objectNode, "interfaceName"))
                .setInterfaceVersion(getStringField(objectNode, "interfaceVersion"))
                .setRequired(getBooleanField(objectNode, "required"))
                .setAuthzScopes(getAuthzScopes(objectNode.withArray("authzScopes")))
                .setSecured(getBooleanField(objectNode,"secured"))
                .setSecuredMethods(getSecuredFields(objectNode.withArray("securedMethods")))
                ;
    }

    @Override
    public String getType() {
        return "SecuredInterface";
    }

    private List<AuthzScope> getAuthzScopes(ArrayNode authzScopeItemsNode) {
        List<AuthzScope> coveredItems = new ArrayList<>();
        for(int i = 0; i < authzScopeItemsNode.size(); i++) {
            if(!authzScopeItemsNode.get(i).isNull()) {
                String name = authzScopeItemsNode.get("name").asText();
                String description = authzScopeItemsNode.get("description").asText();
                coveredItems.add(new AuthzScope(name,description));
            }
        }
        return coveredItems;
    }

    private String getStringField(ObjectNode objectNode, String fieldName) {
        if(!objectNode.has(fieldName)) {
            return null;
        }
        return objectNode.get(fieldName).asText();
    }

    private List<SecuredMethod> getSecuredFields(ArrayNode arrayNode) {
        List<SecuredMethod> coveredItems = new ArrayList<>();
        for(int i = 0; i < arrayNode.size(); i++) {
            if(!arrayNode.get(i).isNull() && arrayNode.get(i).asText().equals("m2m")) {
                coveredItems.add(SecuredMethod.M2M);
            }
        }
        return coveredItems;
    }

    private boolean getBooleanField(ObjectNode objectNode, String fieldName) {
        if(!objectNode.has(fieldName)) {
            return false;
        }
        return objectNode.get(fieldName).asBoolean();
    }
}
