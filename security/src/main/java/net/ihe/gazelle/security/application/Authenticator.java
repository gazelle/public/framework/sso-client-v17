package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.util.Objects;

public interface Authenticator {

   /**
    * Get the weight of the authenticator. The higher the weight is, the closer to the first position in the list the
    * @return the weight of the authenticator
    */
   int getWeight();

   /**
    * Authenticate the entity accessing the application.
    * @return the identity of the accessing entity or an empty identity if the entity is not authenticated
    */
   GazelleIdentity authenticate();

}
