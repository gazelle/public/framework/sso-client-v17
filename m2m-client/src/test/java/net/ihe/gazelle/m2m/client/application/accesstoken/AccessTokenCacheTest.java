package net.ihe.gazelle.m2m.client.application.accesstoken;

import net.ihe.gazelle.m2m.client.interlay.service.AccessTokenCacheService;
import net.ihe.gazelle.m2m.client.mock.MockAccessTokenService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccessTokenCacheTest {

    private static AccessTokenCacheService accessTokenCacheService;

    @BeforeAll
    static void setUp() {
        accessTokenCacheService = new AccessTokenCacheService(new MockAccessTokenService());
    }

    @Test
    void testTokenCache() {
        String token = accessTokenCacheService.getAccessToken("scope");
        assertNotNull(token);

        String tokenFromCache = accessTokenCacheService.getAccessToken("scope");
        assertEquals(token,tokenFromCache);

        String tokenWithDifferentScope = accessTokenCacheService.getAccessToken("differentScope");
        assertNotEquals(token,tokenWithDifferentScope);

        //TODO test expiration
    }

}


