/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay;

import org.eclipse.microprofile.jwt.JsonWebToken;

import java.util.Set;

public class MockedJsonWebToken implements JsonWebToken {

    private Set<String> audiences;

    private final long issuedAtTime;

    private final long expirationTime;

    public MockedJsonWebToken(Set<String> audiences, long issuedAtTime, long expirationTime) {
        this.issuedAtTime = issuedAtTime;
        this.expirationTime = expirationTime;
        if (audiences != null) this.audiences = Set.copyOf(audiences);
    }
    @Override
    public String getName() {
        return null;
    }

    @Override
    public Set<String> getAudience() {
        if (audiences == null) return Set.of();
        return Set.copyOf(audiences);
    }

    @Override
    public long getExpirationTime() {
        return expirationTime;
    }

    @Override
    public long getIssuedAtTime() { return issuedAtTime; }

    @Override
    public Set<String> getClaimNames() {
        return Set.of("sub", "aud", "iat", "exp");
    }

    @Override
    public <T> T getClaim(String s) {
        return null;
    }

}
