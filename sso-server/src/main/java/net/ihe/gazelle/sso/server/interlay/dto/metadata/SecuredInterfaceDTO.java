/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.dto.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.ihe.gazelle.sso.api.domain.metadata.AuthzScope;
import net.ihe.gazelle.sso.api.domain.metadata.SecuredInterface;
import net.ihe.gazelle.sso.api.domain.metadata.SecuredMethod;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.InterfaceDTO;

import java.util.List;

public class SecuredInterfaceDTO extends InterfaceDTO {

    public SecuredInterfaceDTO() {
        super.anInterface = new SecuredInterface();
        super.setType("SecuredInterface");
    }

    @Override
    public SecuredInterfaceDTO setInterfaceName(String interfaceName) {
        super.setInterfaceName(interfaceName);
        return this;
    }

    @Override
    public SecuredInterfaceDTO setInterfaceVersion(String interfaceVersion) {
        super.setInterfaceVersion(interfaceVersion);
        return this;
    }

    @Override
    public SecuredInterfaceDTO setRequired(boolean required) {
        super.setRequired(required);
        return this;
    }

    @Override
    public SecuredInterfaceDTO setBindings(List<BindingDTO> bindingDTOs) {
        super.setBindings(bindingDTOs);
        return this;
    }

    @JsonProperty("authzScopes")
    public List<AuthzScope> getAuthzScopes() {
        return this.getDomainObject().getAuthzScopes();
    }

    public SecuredInterfaceDTO setAuthzScopes(List<AuthzScope> authzScopes) {
        this.getDomainObject().setAuthzScopes(authzScopes);
        return this;
    }

    @JsonProperty("securedMethods")
    public List<String> getSecuredMethods() {
        return this.getDomainObject().getSecuredMethods().stream().map(SecuredMethod::getName).toList();
    }

    public SecuredInterfaceDTO setSecuredMethods(List<SecuredMethod> securedMethods) {
        this.getDomainObject().setSecuredMethods(securedMethods);
        return this;
    }

    @JsonProperty("secured")
    public boolean isSecured() {
        return this.getDomainObject().isSecured();
    }

    public SecuredInterfaceDTO setSecured(boolean secured) {
        this.getDomainObject().setSecured(secured);
        return this;
    }

    @JsonIgnore
    @Override
    public SecuredInterface getDomainObject() {
        return (SecuredInterface) super.getDomainObject();
    }
}
