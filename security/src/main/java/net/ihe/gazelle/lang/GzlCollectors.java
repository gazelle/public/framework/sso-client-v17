package net.ihe.gazelle.lang;

import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class GzlCollectors {

   private GzlCollectors() {
   }

   public static <T> Collector<T, ?, T> toSingletonOrThrow(Supplier<RuntimeException> tooManyItemExceptionSupplier,
         Supplier<RuntimeException> noItemExceptionSupplier) {
      return Collectors.collectingAndThen(
            Collectors.toList(),
            list -> {
               int size = list.size();
               if (size > 1) {
                  throw tooManyItemExceptionSupplier.get();
               } else if (size < 1) {
                  throw noItemExceptionSupplier.get();
               } else {
                  return list.getFirst();
               }
            }
      );
   }
}
