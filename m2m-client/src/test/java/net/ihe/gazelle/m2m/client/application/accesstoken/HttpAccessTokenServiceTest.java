package net.ihe.gazelle.m2m.client.application.accesstoken;

import io.quarkus.test.junit.QuarkusTest;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.m2m.client.interlay.service.resteasy.ClientM2MRegistrationServiceImpl;
import net.ihe.gazelle.m2m.client.interlay.service.apache.HttpKeycloakAccessTokenService;

@QuarkusTest
class HttpAccessTokenServiceTest extends AbstractAccessTokenTest {

    @Override
    protected AccessTokenService getAccessTokenService() {
        ClientM2MRegistrationService clientM2MRegistrationService = new ClientM2MRegistrationServiceImpl(KEYCLOAK_URL, KEYCLOAK_REALM, ADMIN_USER, ADMIN_PASSWORD);
        return new HttpKeycloakAccessTokenService(clientM2MRegistrationService, ssoConfigurationProvider);
    }

}
