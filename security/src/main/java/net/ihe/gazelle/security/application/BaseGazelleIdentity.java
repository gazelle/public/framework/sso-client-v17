package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class BaseGazelleIdentity implements GazelleIdentity {

   private String id;
   private String name;
   private Set<String> groups = new HashSet<>();
   private final Principal principal;

   public BaseGazelleIdentity(Principal principal) {
      this.principal = principal;
      if(principal != null) {
         this.id = principal.getName();
      }
   }

   public static BaseGazelleIdentity unauthenticatedIdentity() {
      return new BaseGazelleIdentity(null);
   }

   @Override
   public String getId() {
      return id;
   }

   public BaseGazelleIdentity setId(String id) {
      this.id = id;
      return this;
   }

   @Override
   public String getName() {
      return name;
   }

   public BaseGazelleIdentity setName(String name) {
      this.name = name;
      return this;
   }

   @Override
   public Set<String> getGroups() {
      return groups;
   }

   public BaseGazelleIdentity setGroups(Set<String> groups) {
      this.groups = groups != null ? groups : new HashSet<>();
      return this;
   }

   @Override
   public String getOrganizationGroup() {
      return getGroups().stream()
            .filter(group -> group.startsWith("org:"))
              .findFirst().orElse(null);
   }

   @Override
   public String getOrganizationId() {
      if (getOrganizationGroup() == null) {
         return null;
      }
      return getOrganizationGroup().replace("org:", "");
   }

   @Override
   public Principal getPrincipal() {
      return principal;
   }

   @Override
   public boolean isAuthenticated() {
      return principal != null && principal.getName() != null;
   }

   @Override
   public boolean hasGroup(String groupName) {
      return getGroups().contains(groupName);
   }
}
