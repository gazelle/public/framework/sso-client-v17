package net.ihe.gazelle.m2m.client.application.registration;

public class ClientM2MRegistrationException extends RuntimeException {

    public ClientM2MRegistrationException() { super(); }

    public ClientM2MRegistrationException(String message) { super(message); }

    public ClientM2MRegistrationException(Throwable cause) {  super(cause); }

    public ClientM2MRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

}
