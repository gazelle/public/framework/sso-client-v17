/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.domain;

public class SSOConstProvider {

    private SSOConstProvider() {}

    public static final String CLAIM_FIRSTNAME = "given_name";
    public static final String CLAIM_LASTNAME = "family_name";
    public static final String CLAIM_EMAIL = "email";
    public static final String CLAIM_AUTHN_METHOD = "authn_method";
    public static final String CLAIM_GROUPS = "groups";
}
