# oidc-server

The OIDC server library is used by the applications which provide protected resources.

## How it works

An OIDC server in Gazelle is a microservice that provides protected resources for other microservices (see [m2m-client](../m2m-client/README.md)).

When a microservice try to get protected resources, it will check the token provided by the client and check if the token is valid (Authentication).

If the token is valid, it will give an instance of the GazelleIdentity to the services. The services has the responsibility to check if the user has the required scopes to access the resource (Authorization).

These verifications are performed by the security framework of Gazelle (see [security](../security/README.md)).

## [GUIDE] Integrate the library

Add required properties in your application.properties

```.properties
gzl.k8s.id=${GZL_XXXX_K8S_ID} # Replace XXXX by a unique identifier for your application
gzl.service.name=${quarkus.application.name} # If necessary, replace by the name of your application
gzl.service.version=${quarkus.application.version}
```

Example of required content for application.properties to works properly with sso-client :

```.properties
gzl.k8s.id=${GZL_GUM_K8S_ID}
gzl.service.name=gazelle-user-management
gzl.service.version=${quarkus.application.version}
```

## [GUIDE] Usage of the library

1. Protect controllers

Add an @ProtectedResource annotation on the methode of your controller that need to be secured.

Example of a secured method in a controller:

```java
public interface ApplicationController {
    @GET
    @Path("/protected")
    @ProtectedResource
    Response getProtectedResource();
}
```

1. Inject Gazelle identity in the implementation of your controller

>:warning: **WARNING:** Your controller must be request scoped. Otherwise, the users will share the same identity.

```java
@RequestScoped
public class ApplicationControllerImpl implements ApplicationController {

  @Inject
  GazelleIdentity identity;

  @Override
  public Response getProtectedResource() {
    authzService.assertAuthorized(identity);
    appService.doSomething();
    return Response.ok().build();
  }
}
```

1. Set environment variables

| Variable name                   | Description                                                                                              | Example                                | Required |
|---------------------------------|----------------------------------------------------------------------------------------------------------|----------------------------------------|----------|
| GZL_SSO_URL                     | The URL of the SSO server.                                                                               | http://localhost:28080                 | true     |
| GZL_SSO_REALM                   | The Keycloak realm used by the SSO.                                                                      | gazelle                                | true     |
| GZL_SSO_ADMIN_USER              | The username/email of the used Keycloak admin.                                                           | admin                                  | true     |
| GZL_SSO_ADMIN_PASSWORD          | The password of the used Keycloak admin.                                                                 | password                               | true     |
| JWT_VERIFY_PUBLIC_KEY_LOCATION  | The location of the public key (url or path).                                                            | http://localhost:28080/realms/gazelle/ | true     |
| JWT_VERIFY_ISSUER               | The location of the issuer of the token.                                                                 | http://localhost:28080/realms/gazelle/ | true     |
| JWT_VERIFY_TRUSTSTORE_KEY_ALIAS | The truststore alias containing the public key (only useful if public key is a path to a truststore).    | password                               | false    |
| JWT_VERIFY_TRUSTSTORE_PASSWORD  | The truststore password containing the public key (only useful if public key is a path to a truststore). | m2m-gazelle-tm                         | false    |

