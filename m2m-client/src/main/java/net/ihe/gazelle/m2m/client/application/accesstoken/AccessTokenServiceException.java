package net.ihe.gazelle.m2m.client.application.accesstoken;


public class AccessTokenServiceException extends RuntimeException {
    public AccessTokenServiceException() {
    }

    public AccessTokenServiceException(String message) {
        super(message);
    }

    public AccessTokenServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessTokenServiceException(Throwable cause) {
        super(cause);
    }
}
