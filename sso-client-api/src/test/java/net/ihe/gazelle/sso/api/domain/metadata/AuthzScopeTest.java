/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.domain.metadata;

import net.ihe.gazelle.sso.api.domain.metadata.AuthzScope;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthzScopeTest {

    @Test
    void testConstructors() {
        AuthzScope authzScope = new AuthzScope();
        authzScope.setName("name");
        authzScope.setDescription("description");
        assertEquals("name", authzScope.getName());
        assertEquals("description", authzScope.getDescription());

        authzScope = new AuthzScope("name2", "description2");
        assertEquals("name2", authzScope.getName());
        assertEquals("description2", authzScope.getDescription());
    }

    @Test
    void testSetters() {
        AuthzScope authzScope = new AuthzScope();
        authzScope.setName("name");
        authzScope.setDescription("description");
        assertEquals("name", authzScope.getName());
        assertEquals("description", authzScope.getDescription());

        authzScope.setName("name2");
        authzScope.setDescription("description2");
        assertEquals("name2", authzScope.getName());
        assertEquals("description2", authzScope.getDescription());
    }

    @Test
    void testCopyConstructor() {
        AuthzScope authzScope = new AuthzScope();
        authzScope.setName("name");
        authzScope.setDescription("description");

        AuthzScope authzScope2 = new AuthzScope(authzScope);
        assertEquals("name", authzScope2.getName());
        assertEquals("description", authzScope2.getDescription());
    }
}
