/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.oidc.server;

import net.ihe.gazelle.security.application.BaseGazelleIdentity;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.time.Instant;

import static net.ihe.gazelle.sso.api.domain.SSOConstProvider.*;

public class OIDCIdentity extends BaseGazelleIdentity {

    private final JsonWebToken jwt;
    private final String firstName;
    private final String lastName;
    private final String email;

    public OIDCIdentity(JsonWebToken jwt) {
        super(jwt);
        this.jwt = jwt;
        this.firstName = jwt.getClaim(CLAIM_FIRSTNAME);
        this.lastName = jwt.getClaim(CLAIM_LASTNAME);
        this.email = jwt.getClaim(CLAIM_EMAIL);
        super.setGroups(jwt.getGroups());
    }

    @Override
    public boolean isAuthenticated() {
        if (jwt == null)
            return false;
        return jwt.getExpirationTime() > Instant.now().getEpochSecond();
    }

    @Override
    public String getName() { return firstName + " " + lastName; }

    public String getEmail() {
        return email;
    }

}