package net.ihe.gazelle.security.application.acl;

import net.ihe.gazelle.security.application.Policy;
import net.ihe.gazelle.security.domain.AccessControlList;
import net.ihe.gazelle.security.domain.GazelleIdentity;
import net.ihe.gazelle.security.domain.MissingContextException;
import net.ihe.gazelle.security.domain.ProtectedResource;

public class DefaultACLPolicies {

   private static final String ROLE_ADMIN = "role:gazelle_admin";
   public static final String MISSING_CONTEXT_EXCEPTION_MESSAGE = "requires the targeted resource to be given in the context for authorization decision.";

   public static class CreatePolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         //FIXME Default resource creation permission should be based on RBAC matrix.
         return true;
      }
   }

   public static class ReadPolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         if (context[0] instanceof ProtectedResource resource) {
            AccessControlList accessControlList = resource.getAccessControlList();
            return accessControlList.isPublic() ||
                  (identity.isAuthenticated() && (
                        accessControlList.isAmongsTheReaders(identity) ||
                              accessControlList.isAmongsTheEditors(identity) ||
                              accessControlList.isAmongsTheOwners(identity) ||
                              identity.hasGroup(ROLE_ADMIN)
                  ));
         } else {
            throw new MissingContextException("ReadPolicy " + MISSING_CONTEXT_EXCEPTION_MESSAGE);
         }
      }
   }

   public static class UpdatePolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         if (context[0] instanceof ProtectedResource resource) {
            AccessControlList accessControlList = resource.getAccessControlList();
            return identity.isAuthenticated() && (
                  accessControlList.isAmongsTheEditors(identity) ||
                        accessControlList.isAmongsTheOwners(identity) ||
                        identity.hasGroup(ROLE_ADMIN)
            );
         } else {
            throw new MissingContextException("UpdatePolicy " + MISSING_CONTEXT_EXCEPTION_MESSAGE);
         }
      }
   }

   public static class DeletePolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         if (context[0] instanceof ProtectedResource resource) {
            AccessControlList accessControlList = resource.getAccessControlList();
            return identity.isAuthenticated() && (
                  accessControlList.isAmongsTheOwners(identity) ||
                        identity.hasGroup(ROLE_ADMIN)
            );
         } else {
            throw new MissingContextException("DeletePolicy " + MISSING_CONTEXT_EXCEPTION_MESSAGE);
         }
      }
   }

   public static class ReadACLPolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         if (context[0] instanceof ProtectedResource resource) {
            AccessControlList accessControlList = resource.getAccessControlList();
            return identity.isAuthenticated() && (
                  accessControlList.isAmongsTheOwners(identity) ||
                        identity.hasGroup(ROLE_ADMIN)
            );
         } else {
            throw new MissingContextException("ReadACLPolicy " + MISSING_CONTEXT_EXCEPTION_MESSAGE);
         }
      }
   }

   public static class UpdateACLPolicy implements Policy {
      @Override
      public boolean evaluate(GazelleIdentity identity, Object... context) {
         if (context[0] instanceof ProtectedResource resource) {
            AccessControlList accessControlList = resource.getAccessControlList();
            return identity.isAuthenticated() && (
                  accessControlList.isAmongsTheOwners(identity) ||
                        identity.hasGroup(ROLE_ADMIN)
            );
         } else {
            throw new MissingContextException("UpdateACLPolicy " + MISSING_CONTEXT_EXCEPTION_MESSAGE);
         }
      }
   }
}
