package net.ihe.gazelle.oidc.server.interlay;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import net.ihe.gazelle.oidc.server.OIDCJWTGenerator;
import org.apache.http.HttpHeaders;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Test;

import java.util.List;

import static net.ihe.gazelle.oidc.server.OIDCJWTGenerator.getValidJwtWithGroups;
import static net.ihe.gazelle.sso.server.interlay.mock.MockRestServer.PROTECTED_ENDPOINT;
import static org.hamcrest.Matchers.containsString;

@QuarkusTest
class AuthorizedRequestTest {

    public static final String BEARER = "Bearer ";

    @Test
    void testWithBadGroupsToken() {
        String validJwt = getValidJwtWithGroups(List.of(""));
        testRequestWithUnauthorizedResponse(BEARER + validJwt);
    }

    @Test
    void testWithBadGroupToken() {
        String validJwt = getValidJwtWithGroups(List.of("bad_groups"));
        testRequestWithUnauthorizedResponse(BEARER + validJwt);
    }

    @Test
    void testWithBadAuthMethodToken() {
        String validJwt = OIDCJWTGenerator.getValidJwtWithAuthzMethod("bad-auth-method");
        testRequestWithUnauthorizedResponse(BEARER + validJwt);
    }

    @Test
    void testCorrectRequest() {
        String validJwt = OIDCJWTGenerator.getValidJwt();
        RestAssured.given()
            .when()
            .header(HttpHeaders.AUTHORIZATION, BEARER + validJwt)
            .get(PROTECTED_ENDPOINT)
            .then()
            .statusCode(RestResponse.StatusCode.OK)
            .body(containsString("\"name\":\"myFirstname myLastname\""))
            .body(containsString("\"organizationsId\":\"kereval\""));
    }

    private void testRequestWithUnauthorizedResponse(String headerValue) {
        RestAssured.given()
                .when()
                .header(HttpHeaders.AUTHORIZATION, headerValue)
                .get(PROTECTED_ENDPOINT)
                .then()
                .statusCode(RestResponse.StatusCode.FORBIDDEN);
    }
}
