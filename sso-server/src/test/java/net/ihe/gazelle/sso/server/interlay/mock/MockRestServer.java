/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.server.interlay.mock;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.security.domain.Authz;
import net.ihe.gazelle.security.domain.GazelleIdentity;
import net.ihe.gazelle.security.domain.UnauthorizedException;
import net.ihe.gazelle.sso.server.interlay.filter.ProtectedResource;

import static net.ihe.gazelle.sso.server.interlay.mock.MockRestServer.PROTECTED_ENDPOINT;

@RequestScoped
@Path(PROTECTED_ENDPOINT)
public class MockRestServer {

    public static final String PROTECTED_ENDPOINT = "/myuser";
    public static final String UNPROTECTED_ENDPOINT = PROTECTED_ENDPOINT + "/unprotected";

    @Inject
    GazelleIdentity gazelleIdentity;

    @Inject
    Authz authz;

    @GET
    @ProtectedResource
    @Produces("application/json")
    public Response getObject() {
        try {
            authz.assertAuthorized(gazelleIdentity, "object:read");
            return Response.ok().entity(identityToJson(gazelleIdentity)).build();
        } catch (UnauthorizedException e) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @GET
    @Produces("application/json")
    @Path("/unprotected")
    public Response getUnprotectedResource() {
        return Response.ok().entity("OK").build();
    }

    private static String identityToJson(GazelleIdentity gazelleIdentity) {
        return "{"
            + "\"id\":\"" + gazelleIdentity.getId() + "\","
            + "\"name\":\"" + gazelleIdentity.getName() + "\","
            + "\"organizationsId\":\"" + gazelleIdentity.getOrganizationId() + "\","
            + "\"groups\":\"" + gazelleIdentity.getGroups().toString() + "\","
            + "}";
    }
}
