package net.ihe.gazelle.security.interlay;

import net.ihe.gazelle.security.application.Authenticator;
import net.ihe.gazelle.security.application.AuthenticatorProvider;
import net.ihe.gazelle.security.application.AuthnImpl;

import java.util.List;
import java.util.ServiceLoader;

public class AuthenticatorSPIProvider implements AuthenticatorProvider {

   @Override
   public List<Authenticator> getAuthenticators() {
      ServiceLoader<Authenticator> serviceLoader = ServiceLoader.load(Authenticator.class);
      return serviceLoader.stream()
            .map(ServiceLoader.Provider::get)
            .sorted(new AuthnImpl.AuthenticatorComparator())
            .toList();
   }

}
