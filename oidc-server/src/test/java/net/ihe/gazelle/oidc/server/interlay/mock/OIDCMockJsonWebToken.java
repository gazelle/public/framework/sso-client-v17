package net.ihe.gazelle.oidc.server.interlay.mock;

import org.eclipse.microprofile.jwt.JsonWebToken;

import java.util.Set;

import static net.ihe.gazelle.sso.api.domain.SSOConstProvider.*;

public class OIDCMockJsonWebToken implements JsonWebToken {
    @Override
    public String getName() {
        return "oidc-test-mocked-jwt";
    }

    @Override
    public long getExpirationTime() { return System.currentTimeMillis() *1000 + 60; }

    @Override
    public Set<String> getClaimNames() {
        return Set.of();
    }

    @Override
    public <T> T getClaim(String s) {
        return switch (s) {
            case CLAIM_FIRSTNAME -> (T) "nom donné";
            case CLAIM_LASTNAME -> (T) "nom de famille";
            case CLAIM_EMAIL -> (T) "email@mocked.fr";
            case CLAIM_GROUPS -> (T) Set.of("admin_role","org:organizationKeyword");
            default -> null;
        };
    }
}
