/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.service.apache.dto;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

public class M2MKeycloakDTOMapper {
    private static final String REALM = "gazelle";
    public static final String FALSE = "false";
    public static final String CLIENT_ID_IS_NULL = "clientId is null";
    private final ObjectMapper mapper;

    public M2MKeycloakDTOMapper() {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

    public String removeReplicaIdFromClientId(String clientId) {
        if (clientId == null)
            throw new IllegalArgumentException(CLIENT_ID_IS_NULL);
        return clientId.replace("m2m-","");
    }

    public String createRealmRoleRepresentationJson(String roleId, String roleName) {
        if (roleName == null)
            throw new IllegalArgumentException("roleName is null");

        RoleDTO roleDTO = new RoleDTO();
        if (roleId != null && !roleId.isEmpty())
            roleDTO.setId(roleId);
        roleDTO.setName(roleName);
        roleDTO.setContainerId(REALM);
        roleDTO.setClientRole(false);

        try {
            return mapper.writeValueAsString(roleDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }

    }

    public String createRealmRoleRepresentationJson(String roleName) {
        return createRealmRoleRepresentationJson("", roleName);
    }

    public String createM2mClientScopeRepresentationJson(String clientScopeName, String description) {
        if (clientScopeName == null)
            throw new IllegalArgumentException("clientScopeName is null");
        if (description == null)
            throw new IllegalArgumentException("description is null");
        M2mClientScopeDTO clientScopeDTO = new M2mClientScopeDTO();
        clientScopeDTO.setName(clientScopeName);
        clientScopeDTO.setDescription(description);
        try {
            return mapper.writeValueAsString(clientScopeDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createProtocolMapperAudienceRepresentationJson(String clientId) {
        return createProtocolMapperAudienceRepresentationJson(clientId, "");
    }

    public String createProtocolMapperAudienceRepresentationJson(String clientId, String protocolMapperId) {
        if (clientId == null)
            throw new IllegalArgumentException(CLIENT_ID_IS_NULL);

        ProtocolMapperDTO protocolMapperDTO = new ProtocolMapperDTO();
        if (protocolMapperId != null && !protocolMapperId.isEmpty())
            protocolMapperDTO.setId(protocolMapperId);
        protocolMapperDTO.setName("audience");
        protocolMapperDTO.setProtocol("openid-connect");
        protocolMapperDTO.setProtocolMapper("oidc-audience-mapper");
        Map<String, String> config = new HashMap<>();
        config.put("id.token.claim", FALSE);
        config.put("access.token.claim", "true");
        config.put("included.custom.audience", clientId);
        config.put("userinfo.token.claim", FALSE);
        protocolMapperDTO.setConfig(config);

        try {
            return mapper.writeValueAsString(protocolMapperDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createProtocolMapperAuthzMethodRepresentationJson() {
        return createProtocolMapperAuthzMethodRepresentationJson("");
    }

    public String createProtocolMapperAuthzMethodRepresentationJson(String protocolMapperId) {
        ProtocolMapperDTO protocolMapperDTO = new ProtocolMapperDTO();
        if (protocolMapperId != null && !protocolMapperId.isEmpty())
            protocolMapperDTO.setId(protocolMapperId);
        protocolMapperDTO.setName("authn_method");
        protocolMapperDTO.setProtocol("openid-connect");
        protocolMapperDTO.setProtocolMapper("oidc-hardcoded-claim-mapper");
        Map<String, String> config = new HashMap<>();
        config.put("id.token.claim", FALSE);
        config.put("access.token.claim", "true");
        config.put("claim.value", "M2M");
        config.put("claim.name", "authn_method");
        config.put("userinfo.token.claim", FALSE);
        protocolMapperDTO.setConfig(config);

        try {
            return mapper.writeValueAsString(protocolMapperDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createClientRepresentationJson(String clientId) {
        if (clientId == null)
            throw new IllegalArgumentException(CLIENT_ID_IS_NULL);

        OidcKeycloakClientDTO clientRepresentation = new OidcKeycloakClientDTO();
        clientRepresentation.setAuthorizationServicesEnabled(false);
        clientRepresentation.setClientAuthenticatorType("client-secret");
        clientRepresentation.setClientId(clientId);
        clientRepresentation.setDirectAccessGrantsEnabled(false);
        clientRepresentation.setName(clientId);

        //these client scopes are created at Keycloak deployment
        clientRepresentation.setDefaultClientScopes(List.of("microprofile-jwt", "m2m-client-scope"));
        clientRepresentation.setPublicClient(false);
        clientRepresentation.setSecret(System.getenv("GZL_M2M_CLIENT_SECRET"));
        clientRepresentation.setServiceAccountsEnabled(true);
        clientRepresentation.setStandardFlowEnabled(false);

        try {
            return mapper.writeValueAsString(clientRepresentation);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }
}
