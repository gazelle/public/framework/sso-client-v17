package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.GazelleIdentity;
import net.ihe.gazelle.security.domain.MissingContextException;


/**
 * Policy is a statement implementation to take the decision to grant or not a permission.
 */
public interface Policy {

   /**
    * Evaluate the policy based on the identity of the requesting entity and the context.
    * <p>
    * Policy evaluated to true tend to authorized access, but keep in mind that they can be composed to create boolean
    * logic and therefore the way the result is interpreted will depend on the permission in which they are used.
    *
    * @param identity Identity of the accessing entity.
    * @param context  the context required to evaluate the policy.
    *
    * @return true or false.
    * @throws MissingContextException if a required object is missing for the evaluation.
    */
   boolean evaluate(GazelleIdentity identity, Object... context);

}
