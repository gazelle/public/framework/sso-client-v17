package net.ihe.gazelle.security.application;

import net.ihe.gazelle.security.domain.Authn;
import net.ihe.gazelle.security.domain.GazelleIdentity;

import java.util.Comparator;
import java.util.List;

public class AuthnImpl implements Authn {

   private final List<Authenticator> orderedAuthenticators;

   public AuthnImpl(AuthenticatorProvider authnProvider) {
      orderedAuthenticators = authnProvider.getAuthenticators();
   }

   @Override
   public GazelleIdentity getIdentity() {
      for (Authenticator authenticator : orderedAuthenticators) {
         GazelleIdentity identity = authenticator.authenticate();
         if (identity.isAuthenticated()) {
            return identity;
         }
      }
      return BaseGazelleIdentity.unauthenticatedIdentity();
   }

   /**
    * Comparator to order Authenticator based on their weight. The higher the weight is, the closer to the first
    * position in the list the authenticator must be.
    */
   public static class AuthenticatorComparator implements Comparator<Authenticator> {
      @Override
      public int compare(Authenticator a1, Authenticator a2) {
         return Integer.compare(a2.getWeight(), a1.getWeight());
      }
   }

}
