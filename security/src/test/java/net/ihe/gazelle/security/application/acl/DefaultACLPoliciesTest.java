/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.security.application.acl;

import net.ihe.gazelle.security.application.BaseGazelleIdentity;
import net.ihe.gazelle.security.application.Policy;
import net.ihe.gazelle.security.application.PrincipalTester;
import net.ihe.gazelle.security.domain.AccessControlList;
import net.ihe.gazelle.security.domain.GazelleIdentity;
import net.ihe.gazelle.security.domain.ProtectedResource;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultACLPoliciesTest {

    private final ProtectedResource ownerProtectedResource = new TesterOwnerProtectedResource();
    private final ProtectedResource editorProtectedResource = new TesterEditorProtectedResource();
    private final ProtectedResource readerProtectedResource = new TesterReaderProtectedResource();

    private final GazelleIdentity gzlAdmin = new BaseGazelleIdentity(new PrincipalTester("non-tester"))
            .setGroups(Set.of("role:gazelle_admin"));
    private final GazelleIdentity gzlTester = new BaseGazelleIdentity(new PrincipalTester("tester"));

    @Test
    void testCreatePolicy() {
        Policy createPolicy = new DefaultACLPolicies.CreatePolicy();
        assertTrue(createPolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(createPolicy.evaluate(gzlTester, ownerProtectedResource)); //TODO based on current WIP implement
        assertTrue(createPolicy.evaluate(gzlTester, editorProtectedResource)); //TODO based on current WIP implement
        assertTrue(createPolicy.evaluate(gzlTester, readerProtectedResource)); //TODO based on current WIP implement
    }

    @Test
    void testReadPolicy() {
        Policy readPolicy = new DefaultACLPolicies.ReadPolicy();
        assertTrue(readPolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(readPolicy.evaluate(gzlTester, ownerProtectedResource));
        assertTrue(readPolicy.evaluate(gzlTester, editorProtectedResource));
        assertTrue(readPolicy.evaluate(gzlTester, readerProtectedResource));
    }

    @Test
    void testUpdatePolicy() {
        Policy updatePolicy = new DefaultACLPolicies.UpdatePolicy();
        assertTrue(updatePolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(updatePolicy.evaluate(gzlTester, ownerProtectedResource));
        assertTrue(updatePolicy.evaluate(gzlTester, editorProtectedResource));
        assertFalse(updatePolicy.evaluate(gzlTester, readerProtectedResource));
    }

    @Test
    void testDeletePolicy() {
        Policy deletePolicy = new DefaultACLPolicies.DeletePolicy();
        assertTrue(deletePolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(deletePolicy.evaluate(gzlTester, ownerProtectedResource));
        assertFalse(deletePolicy.evaluate(gzlTester, editorProtectedResource));
        assertFalse(deletePolicy.evaluate(gzlTester, readerProtectedResource));
    }

    @Test
    void testReadACLPolicy() {
        Policy readACLPolicy = new DefaultACLPolicies.ReadACLPolicy();
        assertTrue(readACLPolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(readACLPolicy.evaluate(gzlTester, ownerProtectedResource));
        assertFalse(readACLPolicy.evaluate(gzlTester, editorProtectedResource));
        assertFalse(readACLPolicy.evaluate(gzlTester, readerProtectedResource));
    }

    @Test
    void testUpdateACLPolicy() {
        Policy updateACLPolicy = new DefaultACLPolicies.UpdateACLPolicy();
        assertTrue(updateACLPolicy.evaluate(gzlAdmin, editorProtectedResource));
        assertTrue(updateACLPolicy.evaluate(gzlTester, ownerProtectedResource));
        assertFalse(updateACLPolicy.evaluate(gzlTester, editorProtectedResource));
        assertFalse(updateACLPolicy.evaluate(gzlTester, readerProtectedResource));
    }

    private static class TesterEditorProtectedResource implements ProtectedResource {
        @Override
        public AccessControlList getAccessControlList() {
            return new AccessControlList().setReadAccessKey("readAccessKey").setEditors(Set.of("tester"));
        }
    }

    private static class TesterOwnerProtectedResource implements ProtectedResource {
        @Override
        public AccessControlList getAccessControlList() {
            return new AccessControlList().setReadAccessKey("readAccessKey").setOwners(Set.of("tester"));
        }
    }

    private static class TesterReaderProtectedResource implements ProtectedResource {
        @Override
        public AccessControlList getAccessControlList() {
            return new AccessControlList().setReadAccessKey("readAccessKey").setReaders(Set.of("tester"));
        }
    }
}
