/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.oidc.server.interlay.mock;

import jakarta.enterprise.context.RequestScoped;
import jakarta.ws.rs.Path;
import net.ihe.gazelle.sso.server.interlay.mock.MockRestServer;

import static net.ihe.gazelle.sso.server.interlay.mock.MockRestServer.PROTECTED_ENDPOINT;

@RequestScoped
@Path(PROTECTED_ENDPOINT)
public class OIDCMockRestServer extends MockRestServer {}
