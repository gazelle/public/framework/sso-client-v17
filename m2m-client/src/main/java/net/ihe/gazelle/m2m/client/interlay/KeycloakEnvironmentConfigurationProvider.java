package net.ihe.gazelle.m2m.client.interlay;

import net.ihe.gazelle.sso.api.application.configuration.SSOConfigurationProvider;


public class KeycloakEnvironmentConfigurationProvider implements SSOConfigurationProvider {
    private final String ssoUrl;

    private final String ssoRealm;

    private final String ssoAdminUser;

    private final String ssoAdminPassword;
    private final String k8sid;
    private final String serviceName;
    private final String clientSecret;

    /**
     * Create new SSOConfigurationProvider that depends on environment variables.
     * @param k8sidVariableName a String of the name of the environment variable that contains the k8s id.
     */
    public KeycloakEnvironmentConfigurationProvider(String k8sidVariableName) {
        ssoAdminPassword = System.getenv("GZL_SSO_ADMIN_PASSWORD");
        ssoAdminUser = System.getenv("GZL_SSO_ADMIN_USER");
        ssoRealm = System.getenv("GZL_SSO_REALM");
        ssoUrl = System.getenv("GZL_SSO_URL");
        k8sid = System.getenv(k8sidVariableName);
        serviceName = System.getenv("GZL_SERVICE_NAME");
        clientSecret = System.getenv("GZL_M2M_CLIENT_SECRET");
    }

    @Override
    public String getSSOUrl() {
        return ssoUrl;
    }

    @Override
    public String getSSORealm() {
        return ssoRealm;
    }

    @Override
    public String getSSOAdminUser() {
        return ssoAdminUser;
    }

    @Override
    public String getSSOAdminPassword() {
        return ssoAdminPassword;
    }

    @Override
    public String getK8sId() {
        return k8sid;
    }

    @Override
    public String getServiceName() {
        return serviceName;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

}
