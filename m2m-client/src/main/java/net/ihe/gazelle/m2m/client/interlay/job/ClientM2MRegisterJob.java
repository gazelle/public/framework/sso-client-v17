/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.m2m.client.interlay.job;

import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import net.ihe.gazelle.m2m.client.application.registration.ClientM2MRegistrationService;
import net.ihe.gazelle.sso.api.interlay.converter.CleanedString;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class ClientM2MRegisterJob {
    private static final Logger log = LoggerFactory.getLogger(ClientM2MRegisterJob.class);
    private static final String MACHINE_ROLE = "machine";
    private static final String ADMIN_ROLE = "role:gazelle_admin";

    private final ClientM2MRegistrationService clientM2MRegistrationService;

    @ConfigProperty(name = "gzl.m2m.registration.startup.enabled")
    Optional<CleanedString> registrationAtStartupEnabled;
    @ConfigProperty(name = "gzl.k8s.id")
    Optional<CleanedString> k8sId;
    @ConfigProperty(name = "gzl.service.name")
    Optional<String> serviceName;
    @ConfigProperty(name = "gzl.m2m.client.secret")
    CleanedString clientSecret;

    @Inject
    public ClientM2MRegisterJob(ClientM2MRegistrationService clientM2MRegistrationService) {
        this.clientM2MRegistrationService = clientM2MRegistrationService;
    }

    void onStart(@Observes StartupEvent ev) {
        if (registrationAtStartupEnabled.isPresent() && Boolean.parseBoolean(registrationAtStartupEnabled.get().getString())) {
            try {
                Set<String> roles = Set.of(MACHINE_ROLE, ADMIN_ROLE);
                clientM2MRegistrationService.registerClient(getClientId(), clientSecret.getString(), roles);
            } catch (Exception e) {
                log.error("Failed to register M2M client to authentication server. This may leads to further issues.", e);
            }
        }
    }

    private String getClientId() {
        // If k8sId is set, use it to compute clientId and clientSecret
        String m2mPrefix = "m2m-";
        if (k8sId.isPresent() && !k8sId.get().getString().isBlank()) {
            String k8sIdCleaned = k8sId.get().getString();
            return m2mPrefix + k8sIdCleaned.substring(0,k8sIdCleaned.lastIndexOf("-"));
            // If serviceName is set, use it to compute clientId and clientSecret
        } else if (serviceName.isPresent() && !serviceName.get().isBlank()) {
            return m2mPrefix + serviceName.get();
        } else {
            throw new IllegalStateException("Either gzl.k8s.id or gzl.service.name properties must be set to compute clientId");
        }
    }
}
