package net.ihe.gazelle.security.domain;

import java.util.Collection;

/**
 * Authorization service to enforce access control for entities accessing the application.
 *
 * @author ceoche
 */
public interface Authz {

   /**
    * Is the given entity authorized to perform the requested action in the given context? The Authorization service
    * will grant the permission of the action based on the evaluation of the associated policies.
    *
    * @param identity Identity of the entity performing the action.
    * @param action   the action that is requested.
    * @param context  the context in which the action has been requested. Different elements may be required to evaluate
    *                 the policies.
    *
    * @return true if the action is authorized, false otherwise.
    *
    * @throws MissingContextException if a context object is required to evaluate the policies but is missing.
    */
   boolean isAuthorized(GazelleIdentity identity, String action, Object... context);

   /**
    * Assert that the given entity is authorized to perform the requested action in the given context. The Authorization
    * service will grant the permission of the action based on the evaluation of the associated policies.
    *
    * @param identity Identity of the entity performing the action.
    * @param action   the action that is requested.
    * @param context  the context in which the action has been requested. Different elements may be required to evaluate
    *                 the policies.
    *
    * @throws UnauthorizedException   if the entity is not granted permission to perform the action.
    * @throws MissingContextException if a context object is required to evaluate the permission but is missing.
    */
   default void assertAuthorized(GazelleIdentity identity, String action, Object... context) {
      if (!isAuthorized(identity, action, context)) {
         throw new UnauthorizedException();
      }
   }

   /**
    * Filter out elements of a collection of protected elements.
    * <p>
    * Authz will check that the accessing entity is authorized to perform the requested action on every element in the
    * collection and discard any elements for which the permission is not granted.
    * <p>
    * <b>Note that, by removing elements, this method may break pagination if called on the result of a search
    * query.</b>
    *
    * @param identity   the identity of the accessing entity.
    * @param action     the requested action.
    * @param collection the collection of protected elements.
    * @param <C>        The type of the protected elements.
    *
    * @return the original collection without any elements for which the permission has not been granted.
    */
   <C extends Collection<? extends Object>> C filterOutUnauthorized(GazelleIdentity identity, String action,
                                                                    C collection);
}
