package net.ihe.gazelle.security.application;

import net.ihe.gazelle.lang.GzlCollectors;

import java.util.Set;

/**
 * Collection of permission (protected actions and associated decision policies). Use permission store to aggregated
 * permissions per use cases or modules in an application.
 * <p>
 * The Authorization framework will aggregate all found permission stores to let client application requesting
 * authorization accesses.
 */
public interface PermissionStore {

   Set<Permission> getPermissions();

   default Permission getPermission(String name) {
      return this.getPermissions().stream().filter(permission -> name.equals(permission.getName()))
            .collect(GzlCollectors.toSingletonOrThrow(
                  () -> new IllegalStateException(String.format(
                        "More than one Permission have been provided for the name '%s'. Only one is allowed",
                        name)),
                  () -> new IllegalStateException(
                        String.format("No Permission found for the name '%s'.", name))
            ));
   }
}
