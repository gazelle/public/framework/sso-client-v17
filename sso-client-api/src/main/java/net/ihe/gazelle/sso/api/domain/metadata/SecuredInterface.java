/*
 * Copyright 2024 IHE International.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.sso.api.domain.metadata;

import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;

import java.util.LinkedList;
import java.util.List;

public class SecuredInterface extends Interface {

    private boolean secured;
    private List<AuthzScope> authzScopes;
    private List<SecuredMethod> securedMethods;

    public SecuredInterface() {
        super();
        this.authzScopes = new LinkedList<>();
        this.securedMethods = new LinkedList<>();
    }

    public SecuredInterface(SecuredInterface securedInterface) {
        super();
        this.secured = securedInterface.isSecured();
        this.authzScopes = new LinkedList<>(securedInterface.getAuthzScopes());
        this.securedMethods = new LinkedList<>(securedInterface.getSecuredMethods());
    }

    public List<AuthzScope> getAuthzScopes() {
        if (authzScopes == null) return List.of();
        return List.copyOf(authzScopes);
    }

    public SecuredInterface setAuthzScopes(List<AuthzScope> authzScopes) {
        this.authzScopes = authzScopes != null ?
                authzScopes :
                new LinkedList<>();
        return new SecuredInterface(this);
    }

    public List<SecuredMethod> getSecuredMethods() {
        if (securedMethods == null) return List.of();
        return List.copyOf(securedMethods);
    }

    public SecuredInterface setSecuredMethods(List<SecuredMethod> securedMethods) {
        this.securedMethods = securedMethods != null ?
                securedMethods :
                new LinkedList<>();
        return new SecuredInterface(this);
    }

    public boolean isSecured() {
        return secured;
    }

    public SecuredInterface setSecured(boolean secured) {
        this.secured = secured;
        return new SecuredInterface(this);
    }
}
